package com.workit.workit.utilities;

import android.app.Activity;
import android.app.AlertDialog;

import com.workit.workit.netUtilities.NetException;

/**
 * Created by Matías on 5/18/2017.
 */

public class AlertHelper {
    private Activity activity;
    private String title;

    public AlertHelper(Activity activity, String title) {
        this.activity = activity;
        this.title = title;
    }

    public void handleError(Throwable error){
        NetException exception = (NetException) error;
        new AlertDialog.Builder(activity).setTitle(title).setMessage(exception.getBodyMessage(error.getMessage())).setPositiveButton("OK",null).show();
    }
}
