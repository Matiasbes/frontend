package com.workit.workit.gcm.configurations;

import rx.Observable;
import rx_gcm.GcmReceiverUIBackground;
import rx_gcm.Message;

/**
 * Created by Matías on 6/15/2017.
 */

public class WorkItGcmReceiverUIBackground implements GcmReceiverUIBackground {

    @Override
    public void onNotification(Observable<Message> oMessage) {
        oMessage.subscribe(message -> WorkItGcmNotificationScheduler.buildAndShowNotification(null, message));
    }

}
