package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.Skill;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/29/2017.
 */

public class SkillService extends NetService<SkillService.SkillServiceApi> {

    public SkillService(Activity activity) {
        super(activity, SkillServiceApi.class);
    }

    public interface SkillServiceApi {

        @GET("skills/{id}")
        Observable<Skill> getSkill(@Path("id") int id);

        @PUT("skills/{id}")
        Observable<Skill> modifySkill(@Path("id") int id, @Body Skill skill);

        @DELETE("skills/{id}")
        Observable<NetSuccess> removeSkill(@Path("id") int id);

    }

    public Observable<Skill> getSkill(int id) {
        Observable<Skill> skill = buildService()
                .getSkill(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return skill;
    }

    public Observable<Skill> modifySkill(int id, Skill dataSkill) {
        Observable<Skill> skill = buildService()
                .modifySkill(id, dataSkill)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return skill;
    }

    public Observable<NetSuccess> removeSkill(int id) {
        Observable<NetSuccess> success = buildService()
                .removeSkill(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return success;
    }

}
