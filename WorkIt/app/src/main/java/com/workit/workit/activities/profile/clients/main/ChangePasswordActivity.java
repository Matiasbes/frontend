package com.workit.workit.activities.profile.clients.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends NotificationActivity {

    @BindView(R.id.actual_password) EditText actualPasswordView;
    @BindView(R.id.actual_password_layout) TextInputLayout actualPasswordLayout;
    @BindView(R.id.new_password) EditText newPasswordView;
    @BindView(R.id.new_password_layout) TextInputLayout newPasswordLayout;
    @BindView(R.id.repeat_new_password) EditText repeatNewPasswordView;
    @BindView(R.id.repeat_new_password_layout) TextInputLayout repeatNewPasswordLayout;

    @BindView(R.id.toolbar) Toolbar toolbar;
    private ProgressDialog progressDialog;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cambiar contraseña");
        setupProgressDialog();
        setupPasswordFields();

        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(ChangePasswordActivity.this);
    }

    private void setupPasswordFields() {
        setupPasswordField(actualPasswordView, actualPasswordLayout);
        setupPasswordField(newPasswordView, newPasswordLayout);
        setupPasswordField(repeatNewPasswordView, repeatNewPasswordLayout);
    }

    private void setupPasswordField(EditText field, TextInputLayout layout) {
        layout.setTypeface(Typeface.DEFAULT);
        field.setTypeface(Typeface.DEFAULT);
        field.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void performChangePasswordAction(View view) {
        String actualPassword = getActualPassword();
        String newPassword = getNewPassword();
        if (actualPassword != null && newPassword != null)
            performNetworkOperation(actualPassword, newPassword);
    }

    private String getActualPassword() {
        String password = actualPasswordView.getText().toString();
        if (password.isEmpty()) {
            actualPasswordView.setError("Este campo es obligatorio");
            return null;
        }
        return password;
    }

    private String getNewPassword() {
        newPasswordView.setError(null);
        String password = newPasswordView.getText().toString();
        if (password.isEmpty()) {
            newPasswordView.setError("Este campo es obligatorio");
            return null;
        }
        String repeatedPassword = getRepeatedNewPassword();
        if (!password.equals(repeatedPassword)) {
            newPasswordView.setError("La nueva contraseña y su verificación deben coincidir");
            return null;
        }
        return password;
    }

    private String getRepeatedNewPassword() {
        String password = repeatNewPasswordView.getText().toString();
        if (password.isEmpty()) {
            repeatNewPasswordView.setError("Este campo es obligatorio");
            return null;
        }
        return password;
    }

    private void performNetworkOperation(String actualPassword, String newPassword) {
        presentProgressDialog("Cambiando contraseña...");
        new UserService(this)
                .changePassword(user, actualPassword, newPassword)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully(User user) {
        Intent intent = new Intent();
        intent.putExtra("user", this.user);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(ChangePasswordActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
