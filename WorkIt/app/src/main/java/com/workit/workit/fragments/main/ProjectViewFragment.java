package com.workit.workit.fragments.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.activities.profile.clients.suppliers.ApplyForProjectActivity;
import com.workit.workit.activities.projects.NewProjectActivity;
import com.workit.workit.activities.projects.ProjectViewActivity;
import com.workit.workit.activities.projects.RateClientActivity;
import com.workit.workit.adapters.projects.ProjectListAdapter;
import com.workit.workit.models.Project;
import com.workit.workit.models.ProjectRating;
import com.workit.workit.models.ProjectStatus;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.services.ProjectService;
import com.workit.workit.utilities.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;

public class ProjectViewFragment extends BaseFragment implements ProjectListAdapter.ProjectsDataProvider {

    @BindView(R.id.add_project) Button addProjectButton;
    @BindView(R.id.looking_for_project) Button lookingForProjectButton;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.fab) FloatingActionButton fab;

    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.empty_projects_layout) LinearLayout emptyProjectsLayout;
    @BindView(R.id.empty_state_description) TextView emptyStateDescription;
    @BindView(R.id.project_list_layout) RelativeLayout projectListLayout;

    private User user;

    private ProjectListAdapter adapter;
    private List<Project> projects;

    public static ProjectViewFragment newInstance(User user) {
        ProjectViewFragment fragment = new ProjectViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.USER_INTENT_KEY, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (User) getArguments().getSerializable(Constants.USER_INTENT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_view, container, false);

        ButterKnife.bind(this, view);
        addProjectButton.setOnClickListener(this::addProject);
        lookingForProjectButton.setOnClickListener(this::lookProject);
        fab.setOnClickListener(user.getType() == UserType.CLIENT ? this::addProject: this::lookProject);
        setupLoadingView();
        updateContent();

        return view;
    }

    private void updateContent() {
        getProjectsAccordingToUserType()
                .subscribe(
                        projects -> {
                            this.projects = projects;
                            updateInterfaceContent();
                        },
                        error -> showErrorMessage());
    }

    private void setupLoadingView() {
        fab.setVisibility(FloatingActionButton.GONE);
        addProjectButton.setVisibility(View.GONE);
        projectListLayout.setVisibility(RelativeLayout.GONE);
        emptyProjectsLayout.setVisibility(LinearLayout.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void updateInterfaceContent() {
        progressBar.setVisibility(View.GONE);
        if(projects.isEmpty()) {
            addProjectButton.setVisibility(user.getType() == UserType.CLIENT ? View.VISIBLE : View.GONE);
            lookingForProjectButton.setVisibility(user.getType() == UserType.SUPPLIER ? View.VISIBLE : View.GONE);
            if (user.getType() == UserType.SUPPLIER)
                emptyStateDescription.setText("No se te ha asignado ningún proyecto todavía");
            projectListLayout.setVisibility(RelativeLayout.GONE);
            fab.setVisibility(FloatingActionButton.GONE);
            emptyProjectsLayout.setVisibility(LinearLayout.VISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
            projectListLayout.setVisibility(RelativeLayout.VISIBLE);
            emptyProjectsLayout.setVisibility(LinearLayout.GONE);
            loadProjects();
        }
    }

    private Observable<List<Project>> getProjectsAccordingToUserType() {
        if (user.getType() == UserType.SUPPLIER)
            return new ProjectService(getActivity()).getSupplierProjects(user.getId());
        return new ProjectService(getActivity()).getClientProjects(user.getId());
    }

    private void loadProjects() {
        adapter = new ProjectListAdapter(getActivity(), this);
        listItems.setAdapter(adapter);

        listItems.setOnItemClickListener((adapterView, view, position, id) -> {
            Project project = (Project) adapterView.getItemAtPosition(position);
            Intent intent = getIntentAccordingToUserType(project);
            intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
            intent.putExtra(Constants.USER_INTENT_KEY, user);
            startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
        });
    }

    private Intent getIntentAccordingToUserType(Project project) {
        if (project.getStatus() == ProjectStatus.Finished && user instanceof Supplier) {
            Supplier supplier = (Supplier) user;
            ProjectRating rating = project.getRatings().get(project.getRatings().indexOf(new ProjectRating(supplier, 0, 0)));
            if (rating.getClientRate() == 0) {
                Intent intent = new Intent(getActivity(), RateClientActivity.class);
                intent.putExtra(Constants.CLIENT_INTENT_KEY, project.getClient());
                return intent;
            }
        }
        return new Intent(getActivity(), ProjectViewActivity.class);
    }

    public void addProject(View view) {
        Intent intent = new Intent(getActivity(), NewProjectActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        startActivityForResult(intent, Constants.ADD_ELEMENT_RETURN_CODE);
    }

    public void lookProject(View view){
        Intent intent = new Intent(getActivity(), ApplyForProjectActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if(user.getType() == UserType.CLIENT) {
            Project project = (Project) data.getExtras().getSerializable(Constants.PROJECT_INTENT_KEY);
            if (projects.contains(project)) {
                projects.remove(project);
                projects.add(project);
            } else
                projects.add(project);
        }
        if(user.getType() == UserType.SUPPLIER){
                 user = (User) data.getExtras().getSerializable(Constants.USER_INTENT_KEY);
                setupLoadingView();
                updateContent();
        }
        if(projects != null && !projects.isEmpty()){
            updateInterfaceContent();
            adapter.notifyDataSetChanged();
            listItems.invalidate();
        }
    }

    private void showErrorMessage() {
        new AlertDialog
                .Builder(getActivity())
                .setTitle("Acción falló")
                .setMessage("La operación no pudo ser realizada")
                .setPositiveButton("OK", (dialog, whichButton) -> getActivity().finish())
                .setCancelable(false)
                .show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        changeActivityTitle("Mis proyectos");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public List<Project> getProjects()  {
        ArrayList<Project> sortedProjects = (ArrayList<Project>) projects;
        Collections.sort(sortedProjects, (project1, project2) -> {
            if (project1.getStatus().getPosition() == project2.getStatus().getPosition())
                return project2.getId() - project1.getId();
            else if (project1.getStatus().getPosition() < project2.getStatus().getPosition())
                return -1;
            else
                return 1;
        });
        return sortedProjects;
    }

    @Override
    public boolean shouldDisplayFabPadding() {
        return user.getType() == UserType.CLIENT;
    }

    @Override
    public boolean shouldDisplayUserRating(Project project) {
        return project.getStatus() == ProjectStatus.Finished && user.getType() == UserType.SUPPLIER;
    }

    @Override
    public int getUserRating(Project project) {
        return project.getRatings().get(project.getRatings().indexOf(new ProjectRating((Supplier) user, 0, 0))).getSupplierRate();
    }

}