package com.workit.workit.activities.projects;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Project;
import com.workit.workit.models.ProjectRating;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ProjectService;
import com.workit.workit.utilities.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateClientActivity extends NotificationActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.content_view) LinearLayout layout;
    private ProgressDialog progressDialog;

    private Project project;
    private User user;
    private User client;

    int rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_client);
        ButterKnife.bind(this);

        project = (Project) getIntent().getSerializableExtra(Constants.PROJECT_INTENT_KEY);
        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        client = (User) getIntent().getSerializableExtra(Constants.CLIENT_INTENT_KEY);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Puntuar cliente");
        toolbar.setNavigationOnClickListener(listener -> finish());
        setupProgressDialog();

        ((TextView) layout.findViewById(R.id.name)).setText(client.getName());
        ((TextView) layout.findViewById(R.id.email)).setText(client.getEmail());
        ((RatingBar) layout.findViewById(R.id.rating_bar)).setOnRatingBarChangeListener((ratingBar, theRating, fromUser) -> rating = (int) theRating);
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(RateClientActivity.this);
    }

    public void performClientRating(View view) {
        presentProgressDialog("Puntuando cliente...");
        new ProjectService(this)
                .rateClient(project.getId(), rating)
                .subscribe(
                        success -> operationFinishedSuccessfully(),
                        this::handleError);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent(this, ProjectViewActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);

        List<ProjectRating> ratings = project.getRatings();
        ProjectRating theRating = ratings.get(ratings.indexOf(new ProjectRating((Supplier) user, 0, 0)));
        theRating.setClientRate(rating);
        ratings.add(theRating);

        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(RateClientActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

}

