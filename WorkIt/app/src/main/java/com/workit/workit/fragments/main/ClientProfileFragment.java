package com.workit.workit.fragments.main;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.models.User;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.Helpers;


public class ClientProfileFragment extends BaseFragment {
    private User user;
    private boolean shouldShowContactActions;

    @BindView(R.id.name) TextView nameView;
    @BindView(R.id.profile_image) SimpleDraweeView profileImage;
    @BindView(R.id.rating_bar) RatingBar ratingBar;
    @BindView(R.id.uses_facebook) TextView usesFacebookView;
    @BindView(R.id.email) TextView emailView;
    @BindView(R.id.phone) TextView phoneView;

    public ClientProfileFragment() {

    }

    public static ClientProfileFragment newInstance(User user, boolean shouldShowContactActions) {
        ClientProfileFragment fragment = new ClientProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.USER_INTENT_KEY, user);
        args.putBoolean("shouldShowContactActions", shouldShowContactActions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (User) getArguments().getSerializable(Constants.USER_INTENT_KEY);
            shouldShowContactActions = getArguments().getBoolean("shouldShowContactActions");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_profile, container, false);
        ButterKnife.bind(this, view);

        updateContent();

        return view;
    }

    private void updateContent() {
        nameView.setText(user.getName());
        usesFacebookView.setText(user.getFacebookId() == null || user.getFacebookId().isEmpty() ? "No asociado a Facebook" : "Asociado a Facebook");
        loadEmailView();
        loadPhoneView();
        loadProfileImage();
        getUserRating();
    }

    private void loadEmailView() {
        emailView.setText(user.getEmail());
        if (shouldShowContactActions) {
            emailView.setTextColor(getResources().getColor(R.color.colorPrimary));
            emailView.setPaintFlags(emailView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            emailView.setOnClickListener(view -> Helpers.loadEmailClients(getActivity(), user.getEmail(), "", ""));
        }
    }

    private void loadPhoneView() {
        phoneView.setText(user.getPhone());
        if (shouldShowContactActions) {
            phoneView.setTextColor(getResources().getColor(R.color.colorPrimary));
            phoneView.setPaintFlags(phoneView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            phoneView.setOnClickListener(view -> Helpers.makeCall(getActivity(), user.getPhone()));
        }
    }

    private void loadProfileImage() {
        Uri uri = getProfilePhotoUri();
        profileImage.setImageURI(uri);
    }

    private Uri getProfilePhotoUri() {
        if (user.getProfilePhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "users/" + user.getId() + "/profileimage");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(user.getProfilePhoto());
    }

    private void getUserRating() {
        new UserService(getActivity())
                .getRating(user.getId())
                .subscribe(
                        rating -> ratingBar.setRating(rating.getRating()),
                        error -> ratingBar.setRating(0));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        changeActivityTitle("Mi perfil");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setUser(User user) {
        this.user = user;
        updateContent();
    }
}
