package com.workit.workit.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;

public class EmptyViewFragment extends Fragment {

    @BindView(R.id.title) TextView titleView;
    @BindView(R.id.description) TextView descriptionView;

    private String title;
    private String description;

    public EmptyViewFragment() {

    }

    public static EmptyViewFragment newInstance(String title, String description) {
        EmptyViewFragment fragment = new EmptyViewFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("description", description);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.title = getArguments().getString("title");
            this.description = getArguments().getString("description");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_empty_view, container, false);

        ButterKnife.bind(this, view);

        titleView.setText(title);
        descriptionView.setText(description);

        return view;
    }

}
