package com.workit.workit.adapters.projects;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.models.Project;

import java.util.List;

/**
 * Created by Matías on 5/31/2017.
 */

public class ProjectListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Activity activity;

    private ProjectsDataProvider dataProvider;

    public ProjectListAdapter(Activity activity, ProjectsDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getProjects().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getProjects().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getProjects().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.project_list_card, null);
        Project project = dataProvider.getProjects().get(position);

        ((TextView) view.findViewById(R.id.title)).setText(project.getTitle());
        ((TextView) view.findViewById(R.id.status)).setText(project.getStatus().getName().toUpperCase());

        boolean shouldDisplayFabPadding = position + 1 == getCount() && dataProvider.shouldDisplayFabPadding();
        view.findViewById(R.id.last_view_padding_from_fab).setVisibility(shouldDisplayFabPadding ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.last_view_padding_from_fab).setOnClickListener(theView -> {});

        if (!dataProvider.shouldDisplayUserRating(project))
            view.findViewById(R.id.rating_container).setVisibility(View.GONE);
        else {
            view.findViewById(R.id.rating_container).setVisibility(View.VISIBLE);
            ((RatingBar) view.findViewById(R.id.rating_bar)).setRating(dataProvider.getUserRating(project));
        }

        return view;
    }

    public interface ProjectsDataProvider {

        List<Project> getProjects();
        boolean shouldDisplayFabPadding();
        boolean shouldDisplayUserRating(Project project);
        int getUserRating(Project project);

    }

}
