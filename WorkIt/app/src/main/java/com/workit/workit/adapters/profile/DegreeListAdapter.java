package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;


import com.workit.workit.R;
import com.workit.workit.models.Degree;

import java.text.SimpleDateFormat;
import java.util.List;
/**
 * Created by Matías on 5/26/2017.
 */

public class DegreeListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;

    private DegreesDataProvider dataProvider;

    public DegreeListAdapter(Activity activity, DegreesDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getDegrees().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getDegrees().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getDegrees().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.degree_card, null);
        Degree degree = dataProvider.getDegrees().get(position);

        ((TextView) view.findViewById(R.id.institution)).setText(degree.getInstitution());
        ((TextView) view.findViewById(R.id.start_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(degree.getStartDate()));
        ((TextView) view.findViewById(R.id.end_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(degree.getEndDate()));
        ((TextView) view.findViewById(R.id.title)).setText(degree.getTitle());

        if (!degree.getDescription().isEmpty())
            ((TextView) view.findViewById(R.id.description)).setText(degree.getDescription());
        else {
            view.findViewById(R.id.description).setVisibility(View.GONE);
            view.findViewById(R.id.description_label).setVisibility(View.GONE);
        }

        view.findViewById(R.id.edit_button)
                .setOnClickListener(theView -> dataProvider.editDegree(degree));
        view.findViewById(R.id.delete_button)
                .setOnClickListener(theView -> dataProvider.removeDegree(degree));

        return view;
    }

    public interface DegreesDataProvider {

        List<Degree> getDegrees();
        void editDegree(Degree degree);
        void removeDegree(Degree degree);

    }

}
