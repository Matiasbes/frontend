package com.workit.workit.fragments.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.adapters.ProfileFragmentAdapter;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.Constants;

public class SupplierProfileFragment extends BaseFragment {
    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.sliding_tabs) TabLayout tabLayout;

    private ProfileFragmentAdapter profileFragmentAdapter;
    private Supplier user;
    private boolean shouldShowContactActions;

    @BindView(R.id.profile_image) SimpleDraweeView profileImage;
    @BindView(R.id.rating_bar) RatingBar ratingBar;
    @BindView(R.id.name) TextView nameView;
    @BindView(R.id.overview) TextView overviewView;

    public SupplierProfileFragment() {

    }

    public static SupplierProfileFragment newInstance(User user, boolean shouldShowContactActions) {
        SupplierProfileFragment fragment = new SupplierProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.USER_INTENT_KEY, user);
        args.putBoolean("shouldShowContactActions", shouldShowContactActions);
        fragment.setArguments(args);
        return fragment;
    }

    private void loadProfileImage() {
        Uri uri = getProfilePhotoUri();
        profileImage.setImageURI(uri);
    }

    private Uri getProfilePhotoUri() {
        if (user.getProfilePhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "users/" + user.getId() + "/profileimage");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(user.getProfilePhoto());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (Supplier) getArguments().getSerializable(Constants.USER_INTENT_KEY);
            shouldShowContactActions = getArguments().getBoolean("shouldShowContactActions", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_supplier_profile, container, false);
        ButterKnife.bind(this, view);

        profileFragmentAdapter = new ProfileFragmentAdapter(getChildFragmentManager(), user, shouldShowContactActions);
        viewPager.setAdapter(profileFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        updateContent();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        changeActivityTitle("Mi perfil");
    }

    public void setUser(User user) {
        this.user = (Supplier) user;
        updateContent();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void updateContent() {
        profileFragmentAdapter.setUser(user);

        loadProfileImage();
        getUserRating();
        setupNameView();
        setupOverviewView();
    }

    private void setupNameView() {
        nameView.setText(user.getName());
    }

    private void setupOverviewView() {
        String overview = user.getOverview();
        if (overview == null || overview.isEmpty())
            overviewView.setVisibility(View.GONE);
        else {
            overviewView.setVisibility(View.VISIBLE);
            overviewView.setText(overview);
        }
    }

    private void getUserRating() {
        new UserService(getActivity())
                .getRating(user.getId())
                .subscribe(
                        rating -> ratingBar.setRating(rating.getRating()),
                        error -> ratingBar.setRating(0));
    }
}
