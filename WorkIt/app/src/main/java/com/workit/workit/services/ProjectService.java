package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.ProjectLocation;
import com.workit.workit.models.Project;
import com.workit.workit.models.ProjectLocationDTO;
import com.workit.workit.models.ProjectRating;
import com.workit.workit.netUtilities.NetHashMap;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/31/2017.
 */

public class ProjectService extends NetService<ProjectService.ProjectServiceApi> {

    public ProjectService(Activity activity) {
        super(activity, ProjectServiceApi.class);
    }

    public interface ProjectServiceApi {

        @GET("suppliers/{id}/projects")
        Observable<List<Project>> getSupplierProjects(@Path("id") int id);

        @GET("clients/{id}/projects")
        Observable<List<Project>> getClientProjects(@Path("id") int id);

        @GET("projects")
        Observable<List<Project>> getProjects();

        @POST("projects/inrange")
        Observable<List<Project>> GetProjectsByDistance( @Body ProjectLocationDTO projectLocationDTO);

        @POST("projects")
        Observable<Project> addProject(@Body Project project);

        @PUT("projects/{id}")
        Observable<Project> modifyProject(@Body Project projectToModify, @Body Project newProject);

        @PUT("projects/{id}/finalize")
        Observable<NetSuccess> finalizeProject(@Path("id") int id, @Body List<ProjectRating> ratings);

        @POST("projects/{id}/rate_client")
        Observable<NetSuccess> rateClient(@Path("id") int id, @Body NetHashMap body);

        @PUT("projects/applay")
        Observable<NetSuccess> applayProjects(@Body List<Project> projects);

        @Multipart
        @PUT("projects/{id}/image")
        Observable<NetSuccess> changePhoto(@Path("id") int id, @Part("images") RequestBody file);
    }

    public Observable<Project> addProject(Project dataProject) {
        Observable<Project> project = buildService()
                .addProject(dataProject)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return project;
    }

    public Observable<Project> modifyProject(Project projectToModify, Project newProject) {
        Observable<Project> project = buildService()
                .modifyProject(projectToModify, newProject)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return project;
    }

    public Observable<List<Project>> getSupplierProjects(int userId) {
        Observable<List<Project>> projects = buildService()
                .getSupplierProjects(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return projects;
    }

    public Observable<List<Project>> getClientProjects(int userId) {
        Observable<List<Project>> projects =  buildService()
                .getClientProjects(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return projects;
    }

    public Observable<List<Project>> getProjects() {
        Observable<List<Project>> projects =  buildService()
                .getProjects()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return projects;
    }

    public Observable<List<Project>> GetProjectsByDistance(Double distance, ProjectLocation projectLocation) {
        ProjectLocationDTO projectLocationDTO = new ProjectLocationDTO(projectLocation.getLatitude(),projectLocation.getLongitude(),distance);
        Observable<List<Project>> projects =  buildService()
                .GetProjectsByDistance(projectLocationDTO)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return projects;
    }

    public Observable<NetSuccess> finalizeProject(int id, List<ProjectRating> ratings) {
        Observable<NetSuccess> success = buildService()
                .finalizeProject(id, ratings)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return success;
    }

    public Observable<NetSuccess> rateClient(int id, int rating) {
        Observable<NetSuccess> success = buildService()
                .rateClient(id, new NetHashMap().append("rating", rating))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return success;
    }

    public Observable<NetSuccess> applayProjects(List<Project> projects) {
        return buildService()
                .applayProjects(projects)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<NetSuccess> changePhoto(@Path("id") int id, File file) {
        RequestBody body = RequestBody.create(MediaType.parse("images/*"), file);
        return buildService()
                .changePhoto(id, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }
}