package com.workit.workit.activities.projects;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.activities.profile.clients.main.ProfileViewActivity;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Project;
import com.workit.workit.models.ProjectStatus;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.Helpers;

public class ProjectViewActivity extends NotificationActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.description) TextView descriptionEditText;
    @BindView(R.id.status) TextView statusEditText;
    @BindView(R.id.budget) TextView budgetEditText;
    @BindView(R.id.deadline) TextView deadlineEditText;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.finalizeProject) Button finalizeProjectButton;
    @BindView(R.id.project_image) SimpleDraweeView projectImage;
    @BindView(R.id.fab) FloatingActionButton fab;
    private Project project;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_view);
        ButterKnife.bind(this);

        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        project = (Project) getIntent().getSerializableExtra(Constants.PROJECT_INTENT_KEY);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> onBackPressed());

        getSupportActionBar().setTitle(project.getTitle());
        setupView();
    }

    private void setupView() {
        descriptionEditText.setText(project.getDescription());
        budgetEditText.setText("$" + Float.toString(project.getBudget()));

        if(project.getDeadline() != null)
            deadlineEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(project.getDeadline()));

        statusEditText.setText(project.getStatus().getName().toUpperCase());

        setupMembersView();
        Helpers.setListViewHeightBasedOnChildren(listItems);

        finalizeProjectButton.setOnClickListener(view -> finalizeProject());
        if(project.getStatus() != ProjectStatus.InProcess || user.getType() != UserType.CLIENT)
            finalizeProjectButton.setVisibility(Button.GONE);
        loadProjectImage();
    }

    private void loadProjectImage() {
        Uri uri = getProjectPhotoUri();
        projectImage.setImageURI(uri);
    }

    private Uri getProjectPhotoUri() {
        if (user.getProfilePhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "projects/" + project.getId() + "/image");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(project.getPhoto());
    }
    private void setupMembersView() {
        String[] members = getSuppliers();
        members[0] = project.getClient().getName();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, members);
        listItems.setAdapter(adapter);
        listItems.setOnItemClickListener((parent, view, position, id) -> showProfile(position));
    }

    private String[] getSuppliers() {
        String[] suppliers = new String[project.getSuppliers().size() + 1];
        for (int i = 1; i <= project.getSuppliers().size(); i++)
            suppliers[i] = project.getSuppliers().get(i - 1).getName();
        return suppliers;
    }

    private void showProfile(int position) {
        User user;
        if (position == 0) {
            user = project.getClient();
            user.setType(UserType.CLIENT.getValue());
        } else {
            user = project.getSuppliers().get(position - 1);
            user.setType(UserType.SUPPLIER.getValue());
        }

        Intent intent = new Intent(this, ProfileViewActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        intent.putExtra("shouldShowContactActions", this.user.getId() != user.getId());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void finalizeProject() {
        Intent intent = new Intent(this, RateMembersActivity.class);
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        startActivity(intent);
    }

    public void showLocation(View view){
        Intent intent = new Intent(view.getContext(),ProjectsMapsActivity.class);
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.putExtra("shouldEnableLocation", false);
        view.getContext().startActivity(intent);
    }
}

