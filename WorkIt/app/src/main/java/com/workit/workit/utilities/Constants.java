package com.workit.workit.utilities;

import android.location.Location;

/**
 * Created by Matías on 5/16/2017.
 */

public class Constants {
    //public static final String BASE_ENDPOINT = "http://192.168.0.105:8080/api/";
    public static final String BASE_ENDPOINT = "http://servicego.myddns.me/WorkIt_v1/api/";


    public static String TOKEN_PREFERENCES_KEY = "Token";
    public static String SHARED_PREFERENCES_NAME = "Preferences";
    public static String USER_TYPE_PREFERENCE_KEY = "UserType";
    public static String USER_ID_PREFERENCE_KEY = "UserId";
    public static String NOTIFICATION_ID_KEY = "NotificationId";

    public static int ADD_ELEMENT_RETURN_CODE = 1000;
    public static int EDIT_ELEMENT_RETURN_CODE = 1001;
    public static int CAMERA_ACTION_RETURN_CODE = 1002;
    public static int GALLERY_ACTION_RETURN_CODE = 1003;
    public static int PERMISSION_REQUEST_CODE = 1004;
    public static int MAP_ACTION_RETURN_CODE = 1005;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    public static String WRITE_EXTERNAL_STORAGE_PERMISSION = "android.permission.WRITE_EXTERNAL_STORAGE";
    public static String READ_EXTERNAL_STORAGE_PERMISSION = "android.permission.READ_EXTERNAL_STORAGE";

    public static String FACEBOOK_PROFILE_ID_KEY = "id";
    public static String FACEBOOK_PROFILE_EMAIL_KEY = "email";

    public static String PROJECT_INTENT_KEY = "project";
    public static String USER_INTENT_KEY = "user";
    public static String TYPE_INTENT_KEY = "type";
    public static String SUPPLIER_INTENT_KEY = "supplier";
    public static String DEGREE_INTENT_KEY = "degree";
    public static String JOB_INTENT_KEY = "job";
    public static String SKILL_INTENT_KEY = "skill";
    public static String PORTFOLIO_INTENT_KEY = "portfolio";
    public static String CLIENT_INTENT_KEY = "client";

    public static String USER_LOCATION = "userLoction";
}
