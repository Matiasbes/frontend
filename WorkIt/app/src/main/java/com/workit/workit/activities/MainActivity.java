package com.workit.workit.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.activities.profile.clients.main.ChangePasswordActivity;
import com.workit.workit.activities.profile.clients.main.EditProfileActivity;
import com.workit.workit.fragments.main.BaseFragment;
import com.workit.workit.fragments.main.ClientProfileFragment;
import com.workit.workit.fragments.main.ProjectViewFragment;
import com.workit.workit.fragments.main.SupplierProfileFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.utilities.AuthenticationHelper;
import com.workit.workit.utilities.Constants;

import butterknife.ButterKnife;
import butterknife.BindView;

public class MainActivity extends NotificationActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        BaseFragment.OnBaseFragmentInteractionListener {

    private static String PROJECT_FRAGMENT_TAG = "project";
    private static String CLIENT_PROFILE_FRAGMENT_TAG = "clientProfile";
    private static String SUPPLIER_PROFILE_FRAGMENT_TAG = "supplierProfile";

    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;

    private int currentMenuItemId;

    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.showOverflowMenu();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        setupDrawerInformation();

        navigationView.setNavigationItemSelectedListener(this);
        onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_project));

        performPermissionRequests();
    }
    private void setupDrawerInformation() {
        View drawerHeaderView = navigationView.getHeaderView(0);

        TextView nameView = (TextView) drawerHeaderView.findViewById(R.id.drawer_name);
        nameView.setText(user.getName());

        TextView emailView = (TextView) drawerHeaderView.findViewById(R.id.drawer_email);
        emailView.setText(user.getEmail());

        loadProfileImage();
    }

    private void loadProfileImage() {
        View drawerHeaderView = navigationView.getHeaderView(0);
        SimpleDraweeView draweeView = (SimpleDraweeView) drawerHeaderView.findViewById(R.id.profile_image);
        draweeView.setImageURI(getProfilePhotoUri());
    }

    private Uri getProfilePhotoUri() {
        if (user.getProfilePhoto() == null)
            return Uri.parse(Constants.BASE_ENDPOINT + "users/" + user.getId() + "/profileimage");
        return Uri.fromFile(user.getProfilePhoto());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_profile:
                Intent editProfileIntent = new Intent(this, EditProfileActivity.class);
                editProfileIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(editProfileIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
            case R.id.action_change_password:
                Intent changePasswordIntent = new Intent(this, ChangePasswordActivity.class);
                changePasswordIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(changePasswordIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
        }
        return true;
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        currentMenuItemId = item.getItemId();
        invalidateOptionsMenu();

        if (currentMenuItemId == R.id.nav_logout){
            logout();
        }
        else {
            replaceContentForItem(currentMenuItemId);
            navigationView.setCheckedItem(currentMenuItemId);
            drawerLayout.closeDrawer(GravityCompat.START);
        }

        return true;
    }

    private void logout() {
        AuthenticationHelper.removeToken(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void replaceContentForItem(int menuId){
        switch (menuId){
            case R.id.nav_project:
                replaceContentWithFragment(ProjectViewFragment.newInstance(user), PROJECT_FRAGMENT_TAG);
                break;
            case R.id.nav_profile:
                if (user.getType() == UserType.CLIENT)
                    replaceContentWithFragment(ClientProfileFragment.newInstance(user, false), CLIENT_PROFILE_FRAGMENT_TAG);
                else
                    replaceContentWithFragment(SupplierProfileFragment.newInstance(user, false), SUPPLIER_PROFILE_FRAGMENT_TAG);
                break;
        }
    }

    private void replaceContentWithFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.baseContentView, fragment, tag)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return currentMenuItemId == R.id.nav_profile;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void changeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != Constants.EDIT_ELEMENT_RETURN_CODE || data == null)
            return;
        user = (User) data.getExtras().getSerializable(Constants.USER_INTENT_KEY);
        setupDrawerInformation();
        loadProfileImage();
        updateChildFragments();
    }

    private void updateChildFragments() {
        SupplierProfileFragment supplierProfile = (SupplierProfileFragment) getSupportFragmentManager().findFragmentByTag(SUPPLIER_PROFILE_FRAGMENT_TAG);
        ClientProfileFragment clientProfile = (ClientProfileFragment) getSupportFragmentManager().findFragmentByTag(CLIENT_PROFILE_FRAGMENT_TAG);

        if (supplierProfile != null)
            supplierProfile.setUser(user);
        if (clientProfile != null)
            clientProfile.setUser(user);
    }

    @TargetApi(23)
    private void performPermissionRequests() {
        if (isMarshmallowOrHigher() && !hasPermissionGranted(Constants.READ_EXTERNAL_STORAGE_PERMISSION))
            requestPermissions(new String[] { Constants.READ_EXTERNAL_STORAGE_PERMISSION, Constants.WRITE_EXTERNAL_STORAGE_PERMISSION }, Constants.PERMISSION_REQUEST_CODE);
    }
    private boolean isMarshmallowOrHigher() {
        return(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    private boolean hasPermissionGranted(String permission) {
        return !isMarshmallowOrHigher() || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

    }
}
