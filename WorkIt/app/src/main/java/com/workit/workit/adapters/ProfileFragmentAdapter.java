package com.workit.workit.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.workit.workit.fragments.main.BaseUserFragment;
import com.workit.workit.fragments.main.ProfileInfoFragment;
import com.workit.workit.fragments.main.ProfilePortfolioFragment;
import com.workit.workit.models.User;

import java.util.HashMap;

/**
 * Created by Matías on 5/25/2017.
 */

public class ProfileFragmentAdapter extends FragmentPagerAdapter {

    private String tabTitles[];
    private User user;
    private boolean shouldShowContactActions;
    private HashMap<Integer, BaseUserFragment> fragments;

    public ProfileFragmentAdapter(FragmentManager fm, User user, boolean shouldShowContactActions) {
        super(fm);
        this.user = user;
        this.shouldShowContactActions = shouldShowContactActions;
        this.fragments = new HashMap<Integer, BaseUserFragment>();
        tabTitles = new String[] { "Portfolio", "Información" };
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (tabTitles[position]) {
            case "Portfolio":
                return ProfilePortfolioFragment.newInstance(user);
            case "Información":
                return ProfileInfoFragment.newInstance(user, shouldShowContactActions);
        }
        return new Fragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public void setUser(User user) {
        this.user = user;
        for(Integer key: fragments.keySet())
            fragments.get(key).updateContent(this.user);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        if (createdFragment instanceof BaseUserFragment) {
            BaseUserFragment baseFragment = (BaseUserFragment) createdFragment;
            fragments.put(position, baseFragment);
            baseFragment.updateContent(user);
        }
        return createdFragment;
    }
}
