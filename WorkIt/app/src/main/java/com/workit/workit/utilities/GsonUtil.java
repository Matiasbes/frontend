package com.workit.workit.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Matías on 5/16/2017.
 */

public class GsonUtil {
    public static Gson getGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
    }
}
