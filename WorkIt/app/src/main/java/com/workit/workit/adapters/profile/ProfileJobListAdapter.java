package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.models.Job;
import com.workit.workit.models.Supplier;

import java.text.SimpleDateFormat;

public class ProfileJobListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Supplier supplier;

    public ProfileJobListAdapter(Activity activity, Supplier supplier) {
        this.supplier = supplier;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return supplier.getJobs().size();
    }

    @Override
    public Object getItem(int position) {
        return supplier.getJobs().get(position);
    }

    @Override
    public long getItemId(int position) {
        return supplier.getJobs().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.job_profile_listing, null);
        Job job = supplier.getJobs().get(position);

        ((TextView) view.findViewById(R.id.enterprise)).setText(job.getEnterprise());
        ((TextView) view.findViewById(R.id.start_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(job.getStartDate()));
        ((TextView) view.findViewById(R.id.end_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(job.getEndDate()));
        ((TextView) view.findViewById(R.id.title)).setText(job.getTitle());

        TextView description = (TextView) view.findViewById(R.id.description);
        if (job.getDescription() != null && !job.getDescription().isEmpty())
            description.setText(job.getDescription());
        else {
            description.setVisibility(View.GONE);
            view.findViewById(R.id.description_label).setVisibility(View.GONE);
        }

        return view;
    }

}

