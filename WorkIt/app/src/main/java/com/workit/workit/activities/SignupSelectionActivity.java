package com.workit.workit.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.workit.workit.R;
import com.workit.workit.models.UserType;
import com.workit.workit.utilities.Constants;

public class SignupSelectionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_selection);
    }
    public void signupClient(View view) {
        presentSignupForm(UserType.CLIENT);
    }

    public void signupSupplier(View view) {
        presentSignupForm(UserType.SUPPLIER);
    }

    private void presentSignupForm(UserType type) {
        Intent intent = new Intent(this, SignupActivity.class);
        intent.putExtra(Constants.TYPE_INTENT_KEY, type);
        startActivity(intent);
    }
}
