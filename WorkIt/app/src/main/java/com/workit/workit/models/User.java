package com.workit.workit.models;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Matías on 5/16/2017.
 */

public class User implements Serializable {
    private int id;
    private String name;
    private String email;
    private String password;
    private UserType type;
    private String phone;
    private String token;
    private String facebookId;
    private File profilePhoto;
    public User(){this.profilePhoto = null;};

    public User(final int id,
                final String name,
                final String email,
                final int type,
                final String token,
                final String facebookId,
                final String phone) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.type = UserType.values()[type];
        this.token = token;
        this.facebookId = facebookId;
        this.profilePhoto = null;
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(int type) {
        this.type = UserType.values()[type];
    }

    public String getFacebookId() {
        return this.facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public File getProfilePhoto() {
        return this.profilePhoto;
    }

    public void setProfilePhoto(File profilePhoto) {
        this.profilePhoto = profilePhoto;
    }
}
