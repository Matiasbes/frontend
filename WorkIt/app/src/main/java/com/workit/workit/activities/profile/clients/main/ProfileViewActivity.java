package com.workit.workit.activities.profile.clients.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.fragments.main.BaseFragment;
import com.workit.workit.fragments.main.ClientProfileFragment;
import com.workit.workit.fragments.main.SupplierProfileFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ClientService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;

public class ProfileViewActivity extends NotificationActivity implements
        BaseFragment.OnBaseFragmentInteractionListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private User user;
    private boolean shouldShowContactActions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> finish());

        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        shouldShowContactActions = getIntent().getBooleanExtra("shouldShowContactActions", false);
        loadUserInfo();
    }
    private void loadUserInfo() {
        if (user.getType() == UserType.CLIENT)
            loadClientInfo();
        else
            loadSupplierInfo();
    }

    private void loadClientInfo() {
        new ClientService(this)
                .getClient(user.getId())
                .subscribe(this::setupView, this::handleError);
    }

    private void loadSupplierInfo() {
        new SupplierService(this)
                .getSupplier(user.getId())
                .subscribe(this::setupView, this::handleError);
    }

    private void setupView(User user) {
        progressBar.setVisibility(View.GONE);
        Fragment newFragment = this.user.getType() == UserType.CLIENT ? ClientProfileFragment.newInstance(user, shouldShowContactActions)
                : SupplierProfileFragment.newInstance(user, shouldShowContactActions);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_view, newFragment, "Profile")
                .commit();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog.Builder(this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", (dialog, whichButton) -> finish())
                .show();
    }

    @Override
    public void changeTitle(String title) {
        getSupportActionBar().setTitle("Perfil de " + user.getName());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
