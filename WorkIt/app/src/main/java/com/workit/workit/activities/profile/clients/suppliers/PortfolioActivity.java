package com.workit.workit.activities.profile.clients.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.PortfolioListAdapter;
import com.workit.workit.fragments.main.EmptyViewFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.PortfolioSample;
import com.workit.workit.models.Supplier;
import com.workit.workit.services.PortfolioService;
import com.workit.workit.utilities.Constants;

public class PortfolioActivity extends NotificationActivity implements PortfolioListAdapter.PortfolioDataProvider {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.empty_state_view) LinearLayout emptyStateView;

    private PortfolioListAdapter adapter;

    private Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);
        ButterKnife.bind(this);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(
                R.id.empty_state_view,
                EmptyViewFragment.newInstance("Sin datos", "No se ha agregado ninguna muestra al portfolio"),
                "emptyView").commit();

        updateInterface();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Portfolio");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> operationFinishedSuccessfully());

        floatingActionButton.setOnClickListener(view -> addPortfolioSample());

        loadPortfolioSamples();
    }

    private void updateInterface() {
        if (supplier.getPortfolio() == null || supplier.getPortfolio().isEmpty()) {
            listItems.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            listItems.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
        }
    }

    private void addPortfolioSample() {
        Intent intent = new Intent(this, PortfolioControllerActivity.class);
        intent.putExtra("supplier", supplier);
        startActivityForResult(intent, Constants.ADD_ELEMENT_RETURN_CODE);
    }

    private void loadPortfolioSamples() {
        adapter = new PortfolioListAdapter(this, this);
        listItems.setAdapter(adapter);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        PortfolioSample portfolioSample = (PortfolioSample) data.getExtras().getSerializable("portfolio");

        if (requestCode == Constants.ADD_ELEMENT_RETURN_CODE)
            supplier.addPortfolioSample(portfolioSample);
        else {
            List<PortfolioSample> portfolio = supplier.getPortfolio();
            portfolio.set(portfolio.indexOf(portfolioSample), portfolioSample);
            supplier.setPortfolio(portfolio);
        }

        updateInterface();
        adapter.notifyDataSetChanged();
        listItems.invalidate();
    }

    public List<PortfolioSample> getPortfolioSamples() {
        return supplier.getPortfolio();
    }

    public void editPortfolioSample(PortfolioSample portfolioSample) {
        Intent intent = new Intent(this, PortfolioControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        intent.putExtra(Constants.PORTFOLIO_INTENT_KEY, portfolioSample);
        startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    public void removePortfolioSample(PortfolioSample portfolioSample) {
        new AlertDialog
                .Builder(this)
                .setTitle("Borrar muestra de trabajo")
                .setMessage("¿Estás seguro de borrar este item?")
                .setPositiveButton("Si", (dialog, whichButton) -> performPortfolioSampleRemoval(portfolioSample))
                .setNegativeButton("No", null)
                .show();
    }

    private void performPortfolioSampleRemoval(PortfolioSample portfolioSample) {
        ProgressDialog progressDialog = presentProgressDialog("Procesando...");
        new PortfolioService(this)
                .removePortfolioSample(portfolioSample.getId())
                .subscribe(
                        message -> {
                            progressDialog.hide();
                            progressDialog.dismiss();

                            removePortfolioSampleFromSupplierData(portfolioSample);
                            updateInterface();
                            adapter.notifyDataSetChanged();
                            listItems.invalidate();
                        },
                        error -> showErrorMessage());
    }

    private ProgressDialog presentProgressDialog(String message) {
        ProgressDialog progressDialog = new ProgressDialog(PortfolioActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    private void removePortfolioSampleFromSupplierData(PortfolioSample portfolioSample) {
        List<PortfolioSample> portfolio = supplier.getPortfolio();
        portfolio.remove(portfolioSample);
        supplier.setPortfolio(portfolio);
    }

    private void showErrorMessage() {
        new AlertDialog
                .Builder(this)
                .setTitle("Acción falló")
                .setMessage("La operación no pudo ser realizada")
                .setPositiveButton("OK", (dialog, whichButton) -> finish())
                .setCancelable(false)
                .show();
    }

    @Override
    public void onBackPressed() {
        operationFinishedSuccessfully();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);
        finish();
    }

}