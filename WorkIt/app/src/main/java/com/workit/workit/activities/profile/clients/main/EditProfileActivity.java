package com.workit.workit.activities.profile.clients.main;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.activities.profile.clients.ClientsInfoActivity;
import com.workit.workit.activities.profile.clients.suppliers.DegreesActivity;
import com.workit.workit.activities.profile.clients.suppliers.JobsActivity;
import com.workit.workit.activities.profile.clients.suppliers.PortfolioActivity;
import com.workit.workit.activities.profile.clients.suppliers.SkillsActivity;
import com.workit.workit.activities.profile.clients.suppliers.SupplierInfoActivity;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.PhotoPickerAlertDialog;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends NotificationActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.options_list) ListView listView;
    @BindView(R.id.actionButton) Button actionButton;
    private ProgressDialog progressDialog;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        ButterKnife.bind(this);

        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        loadProfileImage();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Editar perfil");
        setupProgressDialog();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> operationFinishedSuccessfully());

        setupOptionsList();
        listView.setOnItemClickListener((adapterView, view, position, id) -> handleClick(getMenuForSupplier()[position]));

        actionButton.setVisibility(user.getType() == UserType.CLIENT ? View.VISIBLE : View.GONE);
        listView.setVisibility(user.getType() == UserType.SUPPLIER ? View.VISIBLE : View.GONE);
    }

    private void loadProfileImage() {
        Uri uri = getProfilePhotoUri();
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.profile_image);
        draweeView.setImageURI(uri);
    }

    private Uri getProfilePhotoUri() {
        if (user.getProfilePhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "users/" + user.getId() + "/profileimage");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(user.getProfilePhoto());
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(EditProfileActivity.this);
    }

    private void setupOptionsList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getMenuForSupplier());
        listView.setAdapter(adapter);
    }

    private String[] getMenuForSupplier() {
        return new String[] { "Información básica", "Educación", "Historial laboral", "Portfolio", "Skills" };
    }

    private void handleClick(String menuItem) {
        switch(menuItem) {
            case "Información básica":
                Intent basicInfoIntent = new Intent(this, SupplierInfoActivity.class);
                basicInfoIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(basicInfoIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
            case "Educación":
                Intent degreesIntent = new Intent(this, DegreesActivity.class);
                degreesIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(degreesIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
            case "Historial laboral":
                Intent jobsIntent = new Intent(this, JobsActivity.class);
                jobsIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(jobsIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
            case "Portfolio":
                Intent portfolioIntent = new Intent(this, PortfolioActivity.class);
                portfolioIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(portfolioIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
            case "Skills":
                Intent skillsIntent = new Intent(this, SkillsActivity.class);
                skillsIntent.putExtra(Constants.USER_INTENT_KEY, user);
                startActivityForResult(skillsIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
                break;
        }
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new android.app.AlertDialog.Builder(EditProfileActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    public void performClientEditAction(View view) {
        Intent basicInfoIntent = new Intent(this, ClientsInfoActivity.class);
        basicInfoIntent.putExtra(Constants.USER_INTENT_KEY, user);
        startActivityForResult(basicInfoIntent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    @Override
    public void onBackPressed() {
        operationFinishedSuccessfully();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if (requestCode == Constants.EDIT_ELEMENT_RETURN_CODE) {
            user = (User) data.getExtras().getSerializable(Constants.USER_INTENT_KEY);
        }
        if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE ||
                requestCode == Constants.GALLERY_ACTION_RETURN_CODE) {
            presentProgressDialog("Cargando imagen...");
            File imageFile;

            if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE)
                imageFile = PhotoPickerAlertDialog.imageFromCamera(this, data);
            else
                imageFile = PhotoPickerAlertDialog.imageFromGallery(this, data);

            new UserService(this)
                    .changeProfilePhoto(user.getId(), imageFile)
                    .subscribe(
                            success -> {
                                progressDialog.hide();
                                progressDialog.dismiss();
                                user.setProfilePhoto(imageFile);
                                loadProfileImage();
                            },
                            this::handleError);

        }
    }
    public void changeProfilePhoto(View view) {
        PhotoPickerAlertDialog.showPicker(this, "Cambiar imagen de perfil");
    }
    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
}
