package com.workit.workit.netUtilities;

import android.app.DownloadManager;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Matías on 5/18/2017.
 */

public class NetVolleyQueue {
    private static NetVolleyQueue netVolleyQueueIntance;
    private RequestQueue requestQueue;
    private static Context ctx;

    private NetVolleyQueue(Context context){
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized  NetVolleyQueue getInstance(Context context){
        if(netVolleyQueueIntance == null)
            netVolleyQueueIntance = new NetVolleyQueue(context);
        return netVolleyQueueIntance;
    }

    public RequestQueue getRequestQueue(){
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        return requestQueue;
    }
    public <T> void addToRequestQueue(Request<T> req){
        getRequestQueue().add(req);
    }


}
