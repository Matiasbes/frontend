package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.models.Job;

import java.text.SimpleDateFormat;
import java.util.List;

public class JobListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Activity activity;

    private JobsDataProvider dataProvider;

    public JobListAdapter(Activity activity, JobsDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getJobs().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getJobs().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getJobs().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.job_card, null);
        Job job = dataProvider.getJobs().get(position);

        ((TextView) view.findViewById(R.id.enterprise)).setText(job.getEnterprise());
        ((TextView) view.findViewById(R.id.start_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(job.getStartDate()));
        ((TextView) view.findViewById(R.id.end_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(job.getEndDate()));
        ((TextView) view.findViewById(R.id.title)).setText(job.getTitle());

        if (!job.getDescription().isEmpty())
            ((TextView) view.findViewById(R.id.description)).setText(job.getDescription());
        else {
            view.findViewById(R.id.description).setVisibility(View.GONE);
            view.findViewById(R.id.description_label).setVisibility(View.GONE);
        }

        view.findViewById(R.id.edit_button)
                .setOnClickListener(theView -> dataProvider.editJob(job));
        view.findViewById(R.id.delete_button)
                .setOnClickListener(theView -> dataProvider.removeJob(job));

        return view;
    }

    public interface JobsDataProvider {

        List<Job> getJobs();
        void editJob(Job job);
        void removeJob(Job job);

    }

}
