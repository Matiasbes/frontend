package com.workit.workit.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matías on 5/22/2017.
 */

public class Supplier extends User implements Serializable {
    private String overview;
    private float rate;
    private List<Degree> degrees;
    private List<Job> jobs;
    private List<PortfolioSample> portfolio;
    private List<Skill> skills;

    public Supplier(){
        this.degrees = new ArrayList<>();
        this.jobs = new ArrayList<>();
        this.skills = new ArrayList<>();
    }
    public Supplier(final int id,
                      final String name,
                      final String email,
                      final int type,
                      final String token,
                      final String facebookId,
                      final String overview,
                      final float rate,
                      final List<Degree> degrees,
                      final List<Job> jobs,
                      final List<PortfolioSample> portfolio,
                      final List<Skill> skills,
                      final String phone) {
        super(id, name, email, type, token,facebookId, phone);
        this.overview = overview;
        this.rate = rate;
        this.degrees = degrees;
        this.jobs = jobs;
        this.portfolio = portfolio;
        this.skills = skills;
    }


    public String getOverview() {
        return this.overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public float getRate() {
        return this.rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public List<Degree> getDegrees() {
        return this.degrees;
    }

    public void setDegrees(List<Degree> degrees) {
        this.degrees = degrees;
    }

    public void addDegree(Degree degree) {
        this.degrees.add(degree);
    }

    public List<Job> getJobs() {
        return this.jobs;
    }

    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

    public void addJob(Job job) {
        this.jobs.add(job);
    }

    public List<Skill> getSkills() {
        return this.skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public void addSkill(Skill skill) {
        this.skills.add(skill);
    }

    public List<PortfolioSample> getPortfolio() {
        return this.portfolio;
    }

    public void setPortfolio(List<PortfolioSample> portfolio) {
        this.portfolio = portfolio;
    }

    public void addPortfolioSample(PortfolioSample portfolioSample) {
        this.portfolio.add(portfolioSample);
    }
}
