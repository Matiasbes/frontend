package com.workit.workit.activities;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import rx.Observable;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.workit.workit.R;
import com.workit.workit.gcm.configurations.*;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ClientService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.AuthenticationHelper;
import com.workit.workit.utilities.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.BindView;

public class LoginActivity extends NotificationActivity {

    @BindView(R.id.email) EditText emailView;
    @BindView(R.id.password) EditText passwordView;
    @BindView(R.id.password_text_input) TextInputLayout textInputLayout;
    @BindView(R.id.info)
    TextView info;
    private LoginButton loginButton;
    private ProgressDialog progressDialog;
    private CallbackManager callbackManager;
    private String activityName = "Inicio de Sesión";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        loginButton = (LoginButton)findViewById(R.id.facebook_login);
        setUpFacebook();
        LoginManager.getInstance().logInWithReadPermissions(this,Arrays.asList("email"));
        setUpFacebook();
        ButterKnife.bind(this);
        ButterKnife.setDebug(true);
        setUpInterface();
    }

    private void setUpInterface() {
        setupProgressDialog();
        setUpPasswordView();
    }

    public void performFacebookLogin(View view){
            setUpFacebook();
    }

    private void setUpFacebook() {

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                presentProgressDialog("Iniciando sesión con Facebook...");
                progressDialog.setMessage("Verificando credenciales...");
                RequestData();
            }
            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(),"Login con facebook cancelado",Toast.LENGTH_SHORT);
            }
            @Override
            public void onError(FacebookException e) {
                System.out.println(e.getMessage());
            }
        });
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(LoginActivity.this);
    }

    private void setUpPasswordView() {
        textInputLayout.setTypeface(Typeface.DEFAULT);
        passwordView.setTypeface(Typeface.DEFAULT);
        passwordView.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void performLogin(View view) {
        if (emailView.getText().toString().isEmpty())
            emailView.setError("Este campo es obligatorio");
        else if (passwordView.getText().toString().isEmpty())
            passwordView.setError("Este campo es obligatorio");
        else
            performLoginAction();
    }

    private void performLoginAction() {
        presentProgressDialog("Iniciando sesión...");
        hideKeyboard();
        new UserService(this)
                .login(emailView.getText().toString(), passwordView.getText().toString())
                .flatMap(this::getUserInformation)
                .subscribe(this::userLoggedIn, this::handleLoginError);
    }
    private void handleLoginError(Throwable error) {
        NetException exception = (NetException) error;
        new AlertDialog
                .Builder(LoginActivity.this)
                .setTitle("Inicio de sesión falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }
    private Observable<User> getUserInformation(User user) {
        AuthenticationHelper.saveLoginInformation(this, user);
        if (user.getType() == UserType.CLIENT)
            return getClient(user);
        else if (user.getType() == UserType.SUPPLIER)
            return getSupplier(user);
        return Observable.just(user);
    }

    private Observable<User> getSupplier(User user) {
        return new SupplierService(this)
                .getSupplier(user.getId())
                .map(supplier -> {
                    supplier.setType(UserType.SUPPLIER.getValue());
                    return (User) supplier;
                });
    }

    private Observable<User> getClient(User user) {
        return new ClientService(this)
                .getClient(user.getId())
                .map(client -> {
                    client.setType(UserType.CLIENT.getValue());
                    return (User) client;
                });
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void userLoggedIn(User user) {
        progressDialog.hide();
        progressDialog.dismiss();
        Intent intent = getIntentForUser(user);
        intent.putExtra(Constants.USER_INTENT_KEY,user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private Intent getIntentForUser(User user) {

            return new Intent(this,MainActivity.class);
    }

    public void signup(View view) {
        hideKeyboard();
        progressDialog.dismiss();
        Intent intent = new Intent(this, SignupSelectionActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void attemptLoginWithFacebookCredentials(JSONObject facebookCredentials) {
        try {
            new UserService(this)
                    .facebookLogin(facebookCredentials.getString(Constants.FACEBOOK_PROFILE_ID_KEY),facebookCredentials.getString(Constants.FACEBOOK_PROFILE_EMAIL_KEY))
                    .flatMap(this::getUserInformation)
                    .subscribe(this::userLoggedIn, this::handleLoginError);
        } catch (Exception e) {
            handleLoginError(new NetException("Error en la comunicación con Facebook. Intente más tarde."));
        }
    }

    private void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {
                final JSONObject json = response.getJSONObject();
                if(json != null){
                    attemptLoginWithFacebookCredentials(json);
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
