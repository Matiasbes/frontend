package com.workit.workit.activities.profile.clients.suppliers;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Job;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.JobService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobsControllerActivity extends NotificationActivity {

    private Supplier supplier;
    private Job job;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.actionButton) Button button;

    @BindView(R.id.enterprise) EditText enterpriseEditText;
    @BindView(R.id.start_date) EditText startDateEditText;
    @BindView(R.id.end_date) EditText endDateEditText;
    @BindView(R.id.title) EditText titleEditText;
    @BindView(R.id.description) EditText descriptionEditText;

    private Date startDate;
    private Date endDate;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_controller);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.SUPPLIER_INTENT_KEY);
        job = (Job) getIntent().getSerializableExtra(Constants.JOB_INTENT_KEY);
        setupView();
    }

    private void setupView() {
        setupProgressDialog();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> cancelOperation());

        if (job == null)
            setupViewToAddMode();
        else
            setupViewToModifyMode();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(JobsControllerActivity.this);
    }

    private void cancelOperation() {
        progressDialog.dismiss();
        hideKeyboard();
        finish();
    }

    private void setupViewToAddMode() {
        getSupportActionBar().setTitle("Agregar historial laboral");
        button.setText("Agregar historial laboral");
        GregorianCalendar calendar = new GregorianCalendar();
        setupStartDate(calendar);
        setupEndDate(calendar);
    }

    private void setupStartDate(GregorianCalendar calendar) {
        startDateEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                startDate = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(startDate);
                startDateEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void setupEndDate(GregorianCalendar calendar) {
        endDateEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                endDate = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(endDate);
                endDateEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void setupViewToModifyMode() {
        getSupportActionBar().setTitle("Modificar historial laboral");
        button.setText("Modificar historial laboral");
        startDate = job.getStartDate();
        endDate = job.getEndDate();

        enterpriseEditText.setText(job.getEnterprise());
        startDateEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(startDate));
        endDateEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(endDate));;
        titleEditText.setText(job.getTitle());
        descriptionEditText.setText(job.getDescription());

        GregorianCalendar startDateCalendar = new GregorianCalendar();
        startDateCalendar.setTime(job.getStartDate());
        setupStartDate(startDateCalendar);

        GregorianCalendar endDateCalendar = new GregorianCalendar();
        endDateCalendar.setTime(job.getEndDate());
        setupEndDate(endDateCalendar);
    }

    public void performJobAction(View view) {
        String enterprise = getEnterpriseText();
        Date finalStartDate = getStartDate();
        Date finalEndDate = getEndDate();
        String title = getTitleText();
        String description = descriptionEditText.getText().toString();
        if (enterprise != null && finalStartDate != null && finalEndDate != null && title != null)
            performNetworkAction(new Job(0, enterprise, finalStartDate, finalEndDate, title, description));
    }

    private String getEnterpriseText() {
        String enterprise = enterpriseEditText.getText().toString();
        if (enterprise.isEmpty()) {
            enterpriseEditText.setError("Este campo es obligatorio");
            return null;
        }
        return enterprise;
    }

    private Date getStartDate() {
        if (startDate == null)
            startDateEditText.setError("Este campo es obligatorio");
        return startDate;
    }

    private Date getEndDate() {
        if (endDate == null)
            endDateEditText.setError("Este campo es obligatorio");
        return endDate;
    }

    private String getTitleText() {
        String title = titleEditText.getText().toString();
        if (title.isEmpty()) {
            titleEditText.setError("Este campo es obligatorio");
            return null;
        }
        return title;
    }

    private void performNetworkAction(Job job) {
        presentProgressDialog("Procesando...");
        hideKeyboard();
        if (this.job == null)
            addJob(job);
        else
            modifyJob(job);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void addJob(Job job) {
        new SupplierService(this)
                .addJob(supplier.getId(), job)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void operationFinishedSuccessfully(Job job) {
        Intent intent = new Intent();
        intent.putExtra(Constants.JOB_INTENT_KEY, job);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException) error;
        new AlertDialog
                .Builder(JobsControllerActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    private void modifyJob(Job job) {
        new JobService(this)
                .modifyJob(this.job.getId(), job)
                .subscribe(
                        theJob -> {
                            this.job = theJob;
                            operationFinishedSuccessfully(theJob);
                        },
                        this::handleError);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
