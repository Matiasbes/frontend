package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.Job;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/29/2017.
 */

public class JobService extends NetService<JobService.JobServiceApi> {
    public JobService(Activity activity) {
        super(activity, JobServiceApi.class);
    }

    public interface JobServiceApi {

        @GET("jobs/{id}")
        Observable<Job> getJob(@Path("id") int id);

        @PUT("jobs/{id}")
        Observable<Job> modifyJob(@Path("id") int id, @Body Job job);

        @DELETE("jobs/{id}")
        Observable<NetSuccess> removeJob(@Path("id") int id);

    }

    public Observable<Job> getJob(int id) {
        Observable<Job> job = buildService()
                .getJob(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return job;
    }

    public Observable<Job> modifyJob(int id, Job dataJob) {
        Observable<Job> job =  buildService()
                .modifyJob(id, dataJob)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return job;
    }

    public Observable<NetSuccess> removeJob(int id) {
        Observable<NetSuccess> success = buildService()
                .removeJob(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return success;
    }
}
