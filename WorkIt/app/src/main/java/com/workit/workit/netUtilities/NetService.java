package com.workit.workit.netUtilities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

import com.google.gson.Gson;
import com.workit.workit.activities.LoginActivity;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.GsonUtil;

/**
 * Created by Matías on 5/16/2017.
 */

public class NetService<T> {
    private final Class<T> apiClass;
    private Activity activity;
    private SharedPreferences sharedPreferences;

    public NetService(Activity activity, Class<T> apiClass) {
        this.apiClass = apiClass;
        this.activity = activity;
        sharedPreferences = activity.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, activity.MODE_PRIVATE);
    }

    protected T buildService() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonUtil.getGson()))
                .baseUrl(Constants.BASE_ENDPOINT)
                .client(getHttpClient())
                .build();
        return retrofit.create(apiClass);
    }

    private OkHttpClient getHttpClient() {
        return new OkHttpClient
                .Builder()
                .readTimeout(90, TimeUnit.SECONDS)
                .connectTimeout(90, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    if (sharedPreferences.contains(Constants.TOKEN_PREFERENCES_KEY)) {
                        Request request = chain.request().newBuilder()
                                .header("Authorization", "Basic " + sharedPreferences.getString(Constants.TOKEN_PREFERENCES_KEY, ""))
                                .method(original.method(), original.body())
                                .build();
                        return chain.proceed(request);
                    }

                    return chain.proceed(original);
                })
                .build();
    }
    protected <U> Observable<U> networkErrorEmitter(Throwable error) {
        HttpException exception;
        try {
            exception = (HttpException) error;
        } catch (Exception ex) {
            if (error instanceof IOException) {
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                Toast.makeText(activity, "No tienes conexión a internet", Toast.LENGTH_LONG);
                return buildNetworkException("No tienes conexión a internet.");
            }
            return buildNetworkException("El servidor está ocupado, intente luego.");
        }
        return buildNetworkError(exception);
    }

    private <U> Observable<U> buildNetworkException(String message) {
        return Observable.error(new NetException(message));
    }

    private <U> Observable<U> buildNetworkError(HttpException exception) {
        try {
            String jsonMessage = exception.response().errorBody().string();
            String message = new Gson().fromJson(jsonMessage, NetError.class).getMessage();
            return buildNetworkException(message);
        } catch (IOException ex) {
            return buildNetworkException("El servidor está ocupado, intente luego.");
        }
    }



}
