package com.workit.workit.gcm.model;

import com.workit.workit.models.Project;
import com.workit.workit.models.User;

/**
 * Created by Matías on 6/15/2017.
 */

public class Notification {

    private NotificationType type;
    private String title;
    private String description;
    private User user;
    private Project project;
    private User client;

    public Notification(final int type,
                        final String title,
                        final String description,
                        final User user,
                        final Project project,
                        final User client) {
        this.type = NotificationType.values()[type];
        this.title = title;
        this.description = description;
        this.user = user;
        this.project = project;
        this.client = client;
    }

    public NotificationType getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = NotificationType.values()[type];
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return this.project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getClient() {
        return this.client;
    }

    public void setClient(User client) {
        this.client = client;
    }

}
