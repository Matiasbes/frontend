package com.workit.workit.adapters.projects;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.activities.projects.ProjectViewActivity;
import com.workit.workit.models.Project;
import com.workit.workit.utilities.Constants;

import java.util.List;

/**
 * Created by Matías on 6/16/2017.
 */

public class ProjectsSupplierAssignmentListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;

    private ProjectsSupplierAssignmentDataProvider dataProvider;

    public ProjectsSupplierAssignmentListAdapter(Activity activity, ProjectsSupplierAssignmentDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return dataProvider.getProjectsForSupplierAssignment().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getProjectsForSupplierAssignment().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getProjectsForSupplierAssignment().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.projects_assignment_listing, null);

        Project project = dataProvider.getProjectsForSupplierAssignment().get(position);

        ((TextView) view.findViewById(R.id.project_title)).setText(project.getTitle());
        ((TextView) view.findViewById(R.id.project_client)).setText(project.getClient().getEmail());

        SimpleDraweeView selectedView = (SimpleDraweeView) view.findViewById(R.id.selected_view);
        selectedView.setVisibility(View.GONE);
        view.setOnClickListener(theView -> {
            if (selectedView.getVisibility() == View.GONE) {
                selectedView.setVisibility(View.VISIBLE);
                dataProvider.projectSelected(project);
            } else {
                selectedView.setVisibility(View.GONE);
                dataProvider.projectDeselected(project);
            }
        });
        view.setOnLongClickListener(theView -> {
            Intent intent = new Intent(view.getContext(), ProjectViewActivity.class);
            intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
            intent.putExtra(Constants.USER_INTENT_KEY, project.getClient());
            ((Activity)view.getContext()).startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
            return true;
        });
        return view;
    }


    public interface ProjectsSupplierAssignmentDataProvider {

        List<Project> getProjectsForSupplierAssignment();
        void projectSelected(Project project);
        void projectDeselected(Project project);

    }
}
