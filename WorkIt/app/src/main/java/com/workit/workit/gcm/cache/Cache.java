package com.workit.workit.gcm.cache;

import com.workit.workit.gcm.model.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matías on 6/15/2017.
 */

public enum Cache {

    Pool;

    private final List<Notification> notifications = new ArrayList<>();

    public void addNotification(Notification notification) { notifications.add(notification); }
    public List<Notification> getNotifications() { return this.notifications; }

}
