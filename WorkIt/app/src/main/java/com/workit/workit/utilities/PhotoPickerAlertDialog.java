package com.workit.workit.utilities;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Matías on 6/13/2017.
 */

public class PhotoPickerAlertDialog {
    private static final CharSequence[] items = { "Sacar foto", "Seleccionar desde libreria", "Cancelar" };

    public static void showPicker(Activity activity, String title) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Sacar foto")) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                activity.startActivityForResult(intent, Constants.CAMERA_ACTION_RETURN_CODE);
            } else if (items[item].equals("Seleccionar desde libreria")) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                activity.startActivityForResult(Intent.createChooser(intent, "Seleccionar imagen"), Constants.GALLERY_ACTION_RETURN_CODE);
            } else if (items[item].equals("Cancelar")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static File imageFromCamera(Activity activity, Intent data) {
        File file = new File(activity.getCacheDir(), System.currentTimeMillis() + ".jpg");

        try {
            file.createNewFile();
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
            byte[] bitmapdata = byteStream.toByteArray();
            FileOutputStream fileStream = new FileOutputStream(file);
            fileStream.write(bitmapdata);
            fileStream.flush();
            fileStream.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return file;
    }

    public static File imageFromGallery(Activity activity, Intent data) {
        Uri baseUri = data.getData();

        String[] projection = { MediaStore.MediaColumns.DATA };
        CursorLoader cursorLoader = new CursorLoader(activity, baseUri, projection, null, null, null);
        Cursor cursor =cursorLoader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String selectedImagePath = cursor.getString(column_index);

        Uri selectedUri = Uri.parse(selectedImagePath);
        return new File(selectedUri.getPath());
    }

}
