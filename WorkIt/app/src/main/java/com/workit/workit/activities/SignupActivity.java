package com.workit.workit.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ClientService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.services.UserService;
import com.workit.workit.utilities.AuthenticationHelper;
import com.workit.workit.utilities.Constants;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.BindView;
import rx.Observable;

public class SignupActivity extends NotificationActivity {

    @BindView(R.id.name) EditText nameView;
    @BindView(R.id.email) EditText emailView;
    @BindView(R.id.password) EditText passwordView;
    @BindView(R.id.password_text_input) TextInputLayout passwordInputLayout;

    private ProgressDialog progressDialog;
    private User user;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_signup);
        loginButton = (LoginButton)findViewById(R.id.facebook_login);
        user = new User();
        UserType type = (UserType) getIntent().getSerializableExtra("type");
        user.setType(type.getValue());

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
        setUpLoginFacebook();
        ButterKnife.bind(this);
        setUpInterface();
    }

    private void setUpInterface() {
        setUpProgressDialog();
        setUpPasswordView();

    }

    public void performFacebookSignup(View view){
            setUpLoginFacebook();
    }

    private void setUpLoginFacebook() {
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                presentProgressDialog("Registrando con Facebook...");
                progressDialog.setMessage("Verificando credenciales...");
                RequestData();
            }
            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(),"Registro con facebook cancelado",Toast.LENGTH_SHORT);
            }
            @Override
            public void onError(FacebookException e) {
                System.out.println(e.getMessage());
            }
        });
    }

    private void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {
                final JSONObject json = response.getJSONObject();
                if(json != null){
                    attemptSignupWithFacebookCredentials(json);
                }
            }
        });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }
    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(SignupActivity.this);
    }

    private void setUpPasswordView() {
        passwordInputLayout.setTypeface(Typeface.DEFAULT);
        passwordView.setTypeface(Typeface.DEFAULT);
        passwordView.setTransformationMethod(new PasswordTransformationMethod());
    }
    public void performSignup(View view) {
        if (nameView.getText().toString().isEmpty())
            nameView.setError("Este campo es obligatorio");
        else if (emailView.getText().toString().isEmpty())
            emailView.setError("Este campo es obligatorio");
        else if (passwordView.getText().toString().isEmpty())
            passwordView.setError("Este campo es obligatorio");
        else
            performSignupAction();
    }
    private void performSignupAction() {
        presentProgressDialog("Registrando...");
        hideKeyboard();
        prepareDataForSignup();
        new UserService(this)
                .signup(user)
                .flatMap(this::getUserInformation)
                .subscribe(this::userSignupSucceded, this::handleLoginError);
    }
    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
    private void prepareDataForSignup() {
        user.setName(nameView.getText().toString());
        user.setEmail(emailView.getText().toString());
        user.setPassword(passwordView.getText().toString());
    }
    public void login(View view) {
        hideKeyboard();
        progressDialog.dismiss();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public Observable<User> getUserInformation(User user) {
        AuthenticationHelper.saveLoginInformation(this, user);
        if (user.getType() == UserType.CLIENT)
            return getClient(user);
        return getSupplier(user);
    }

   private Observable<User> getSupplier(User user) {
        return new SupplierService(this)
                .getSupplier(user.getId())
                .map(supplier -> {
                    supplier.setType(UserType.SUPPLIER.getValue());
                    return (User) supplier;
                });
    }

    private Observable<User> getClient(User user) {
        return new ClientService(this)
                .getClient(user.getId())
                .map(client -> {
                    client.setType(UserType.CLIENT.getValue());
                    return (User) client;
                });
    }
    private void userSignupSucceded(User user) {
        progressDialog.hide();
        progressDialog.dismiss();
        Intent intent = new Intent(this,MainActivity.class);

        intent.putExtra("user", user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    private void handleLoginError(Throwable error) {
        NetException exception = (NetException) error;
        new AlertDialog.Builder(SignupActivity.this)
                .setTitle("Registro falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void attemptSignupWithFacebookCredentials(JSONObject facebookCredentials) {
        try {
            user.setFacebookId(facebookCredentials.getString("id"));
            user.setName(facebookCredentials.getString("name"));
            user.setEmail(facebookCredentials.getString("email"));
            new UserService(this)
                    .facebookSignup(user)
                    .flatMap(this::getUserInformation)
                    .subscribe(this::userSignupSucceded, this::handleLoginError);
        } catch (Exception e) {
            handleLoginError(new NetException("Error en la comunicación con Facebook. Intente más tarde."));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
