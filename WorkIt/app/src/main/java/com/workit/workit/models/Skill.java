package com.workit.workit.models;

import java.io.Serializable;

/**
 * Created by Matías on 5/22/2017.
 */

public class Skill implements Serializable {
    private int id;
    private String title;

    public Skill() {

    }

    public Skill(final int id,
                 final String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof Skill) {
            Skill object = (Skill) anObject;
            return this.getId() == object.getId();
        }
        return false;
    }
}
