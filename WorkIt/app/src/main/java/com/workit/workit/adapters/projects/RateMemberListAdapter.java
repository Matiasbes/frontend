package com.workit.workit.adapters.projects;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.models.User;
import com.workit.workit.utilities.Constants;

import java.util.List;

/**
 * Created by Matías on 5/31/2017.
 */

public class RateMemberListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private ProjectMemberDataProvider dataProvider;

    public RateMemberListAdapter(Activity activity, ProjectMemberDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getMembers().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getMembers().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getMembers().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.rate_member_card, null);
        User user = dataProvider.getMembers().get(position);

        loadImage(view, user);
        ((TextView) view.findViewById(R.id.name)).setText(user.getName());
        ((TextView) view.findViewById(R.id.email)).setText(user.getEmail());

        ((RatingBar) view.findViewById(R.id.rating_bar)).setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> dataProvider.userRated(user, rating));

        return view;
    }

    private void loadImage(View superview, User user) {
        Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "users/" + user.getId() + "/profileimage");
        SimpleDraweeView draweeView = (SimpleDraweeView) superview.findViewById(R.id.profile_image);
        draweeView.setImageURI(uri);
    }

    public interface ProjectMemberDataProvider {

        List<User> getMembers();
        void userRated(User user, float rate);

    }

}
