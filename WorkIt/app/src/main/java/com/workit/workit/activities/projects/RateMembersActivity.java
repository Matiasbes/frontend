package com.workit.workit.activities.projects;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.workit.workit.R;
import com.workit.workit.activities.MainActivity;
import com.workit.workit.adapters.projects.RateMemberListAdapter;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Project;
import com.workit.workit.models.ProjectRating;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ProjectService;
import com.workit.workit.utilities.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RateMembersActivity extends NotificationActivity implements RateMemberListAdapter.ProjectMemberDataProvider{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.list_items) ListView listItems;

    private Project project;
    private User user;

    private ProgressDialog progressDialog;
    private RateMemberListAdapter adapter;

    private List<ProjectRating> ratings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_members);
        ButterKnife.bind(this);

        project = (Project) getIntent().getSerializableExtra(Constants.PROJECT_INTENT_KEY);
        user = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        ratings = new ArrayList<>();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Puntuar miembros");
        toolbar.setNavigationOnClickListener(listener -> finish());
        setupProgressDialog();
        setupView();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(RateMembersActivity.this);
    }

    private void setupView() {
        adapter = new RateMemberListAdapter(this, this);
        listItems.setAdapter(adapter);
    }

    public List<User> getMembers() {
        return project.getSuppliers();
    }

    public void userRated(User user, float rate) {
        Supplier supplier = new Supplier(user.getId(),null,null,0,null,null,null,00.f,null,null,null,null,null);
        ProjectRating rating = new ProjectRating(supplier, 0, (int) rate);
        if (ratings.contains(rating))
            ratings.get(ratings.indexOf(rating)).setSupplierRate((int) rate);
        else
            ratings.add(rating);
    }

    public void performProjectFinalization(View view) {
        presentProgressDialog("Finalizando proyecto...");
        new ProjectService(this)
                .finalizeProject(project.getId(), ratings)
                .subscribe(
                        success -> operationFinishedSuccessfully(),
                        this::handleError);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(RateMembersActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}