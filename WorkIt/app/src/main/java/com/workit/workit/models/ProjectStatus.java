package com.workit.workit.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matías on 5/31/2017.
 */

public enum  ProjectStatus {
    Pending("Pendiente"),
    InProcess("En proceso"),
    Finished("Finalizado");

    private final String name;

    private static final Map<String, ProjectStatus> lookup = new HashMap<String, ProjectStatus>();

    static {
        for (ProjectStatus status : ProjectStatus.values())
            lookup.put(status.getName(), status);
    }

    private ProjectStatus(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public static ProjectStatus get(String name) {
        return lookup.get(name);
    }

    public int getPosition() {
        switch (this) {
            case Pending:
                return 1;
            case InProcess:
                return 0;
            case Finished:
                return 2;
            default:
                return 3;
        }
    }
}
