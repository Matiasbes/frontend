package com.workit.workit.gcm.configurations;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.gson.Gson;

import rx_gcm.Message;

import com.workit.workit.R;
import com.workit.workit.activities.StartActivity;
import com.workit.workit.activities.projects.ProjectViewActivity;
import com.workit.workit.activities.projects.RateClientActivity;
import com.workit.workit.gcm.model.Notification;
import com.workit.workit.gcm.model.NotificationType;
import com.workit.workit.models.Client;
import com.workit.workit.models.Project;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.GsonUtil;

/**
 * Created by Matías on 6/15/2017.
 */

public class WorkItGcmNotificationScheduler {
    private static SharedPreferences sharedPreferences;

    public static Notification parseNotificationFromGcmMessage(Message message) {
        Bundle payload = message.payload();
        Gson gson = GsonUtil.getGson();

        int type = Integer.parseInt(message.target());
        String title = payload.getString("title");
        String description = payload.getString("description");

        User user = gson.fromJson(payload.getString(Constants.USER_INTENT_KEY), User.class);
        if (user.getType() == UserType.SUPPLIER)
            user = gson.fromJson(payload.getString(Constants.USER_INTENT_KEY), Supplier.class);
        if (user.getType() == UserType.CLIENT)
            user = gson.fromJson(payload.getString(Constants.USER_INTENT_KEY), Client.class);

        Project project = gson.fromJson(payload.getString(Constants.PROJECT_INTENT_KEY), Project.class);
        User client = gson.fromJson(payload.getString(Constants.CLIENT_INTENT_KEY), User.class);

        return new Notification(type, title, description, user, project, client);
    }

    public static void buildAndShowNotification(Activity activity, Message message) {
        Application application = message.application();
        sharedPreferences = application.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        Notification notification = parseNotificationFromGcmMessage(message);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat
                .Builder(application)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getDescription())
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_briefcase_logo)
                .setGroup("workit")
                .setAutoCancel(true)
                .setContentIntent(getPendingIntentForNotification(application, activity, notification));

        NotificationManager notificationManager = (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(getNotificationId(sharedPreferences), notificationBuilder.build());
    }

    private static PendingIntent getPendingIntentForNotification(Application application, Activity activity, Notification notification) {
        Class<? extends Activity> classActivity = getActivityClassForNotificationType(notification.getType());

        Intent resultIntent;
        TaskStackBuilder stackBuilder;
        if (activity == null) {
            resultIntent = new Intent(application, classActivity);
            stackBuilder = TaskStackBuilder.create(application);
        } else {
            resultIntent = new Intent(activity, classActivity);
            stackBuilder = TaskStackBuilder.create(activity);
        }

        resultIntent.putExtra(Constants.USER_INTENT_KEY, notification.getUser());
        resultIntent.putExtra(Constants.PROJECT_INTENT_KEY, notification.getProject());
        resultIntent.putExtra(Constants.CLIENT_INTENT_KEY, notification.getClient());

        stackBuilder.addParentStack(classActivity);
        stackBuilder.addNextIntent(resultIntent);

        Intent backIntent = stackBuilder.editIntentAt(0);
        if (backIntent != null)
            backIntent.putExtra(Constants.USER_INTENT_KEY, notification.getUser());

        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Class<? extends Activity> getActivityClassForNotificationType(NotificationType notificationType) {
        boolean isLoggedIn = sharedPreferences.contains(Constants.TOKEN_PREFERENCES_KEY);
        if (!isLoggedIn)
            return StartActivity.class;

        switch (notificationType) {
            case APPROVED_PROJECT:
                return ProjectViewActivity.class;
            case FINALIZED_PROJECT:
                return RateClientActivity.class;
            case CLIENT_RATED:
                return ProjectViewActivity.class;
            default:
                return null;
        }
    }

    private static int getNotificationId(SharedPreferences preferences) {
        int currentId = preferences.getInt(Constants.NOTIFICATION_ID_KEY, 1);
        sharedPreferences.edit().putInt(Constants.NOTIFICATION_ID_KEY, currentId + 1).commit();
        return currentId;
    }
}
