package com.workit.workit.gcm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matías on 6/15/2017.
 */

public enum NotificationType {

    @SerializedName("0")
    APPROVED_PROJECT(0),

    @SerializedName("1")
    FINALIZED_PROJECT(1),

    @SerializedName("2")
    CLIENT_RATED(2);

    private final int value;

    NotificationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}

