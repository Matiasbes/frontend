package com.workit.workit.activities.profile.clients.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.JobListAdapter;
import com.workit.workit.fragments.main.EmptyViewFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Job;
import com.workit.workit.models.Supplier;
import com.workit.workit.services.JobService;
import com.workit.workit.utilities.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobsActivity extends NotificationActivity implements JobListAdapter.JobsDataProvider {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.empty_state_view) LinearLayout emptyStateView;

    private JobListAdapter adapter;

    private Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);
        ButterKnife.bind(this);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(
                R.id.empty_state_view,
                EmptyViewFragment.newInstance("Sin datos", "No ha ingresado su historial laboral"),
                "emptyView").commit();

        updateInterface();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Historial laboral");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> operationFinishedSuccessfully());

        floatingActionButton.setOnClickListener(view -> addJob());

        loadJobs();
    }

    private void updateInterface() {
        if (supplier.getJobs() == null || supplier.getJobs().isEmpty()) {
            listItems.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            listItems.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
        }
    }

    private void addJob() {
        Intent intent = new Intent(this, JobsControllerActivity.class);
        intent.putExtra("supplier", supplier);
        startActivityForResult(intent, Constants.ADD_ELEMENT_RETURN_CODE);
    }

    private void loadJobs() {
        adapter = new JobListAdapter(this, this);
        listItems.setAdapter(adapter);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        Job job = (Job)data.getExtras().getSerializable(Constants.JOB_INTENT_KEY);

        if (requestCode == Constants.ADD_ELEMENT_RETURN_CODE)
            supplier.addJob(job);
        else {
            List<Job> jobs = supplier.getJobs();
            jobs.set(jobs.indexOf(job), job);
            supplier.setJobs(jobs);
        }

        updateInterface();
        adapter.notifyDataSetChanged();
        listItems.invalidate();
    }

    public List<Job> getJobs() {
        return supplier.getJobs();
    }

    public void editJob(Job job) {
        Intent intent = new Intent(this, JobsControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        intent.putExtra(Constants.JOB_INTENT_KEY, job);
        this.startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    public void removeJob(Job job) {
        new AlertDialog
                .Builder(this)
                .setTitle("Borrar historial laboral")
                .setMessage("¿Estás seguro de borrar este item?")
                .setPositiveButton("Si", (dialog, whichButton) -> performJobRemoval(job))
                .setNegativeButton("No", null)
                .show();
    }

    private void performJobRemoval(Job job) {
        ProgressDialog progressDialog = presentProgressDialog("Procesando...");
        new JobService(this)
                .removeJob(job.getId())
                .subscribe(
                        message -> {
                            progressDialog.hide();
                            progressDialog.dismiss();

                            removeJobFromSupplierData(job);
                            updateInterface();
                            adapter.notifyDataSetChanged();
                            listItems.invalidate();
                        },
                        error -> showErrorMessage());
    }

    private ProgressDialog presentProgressDialog(String message) {
        ProgressDialog progressDialog = new ProgressDialog(JobsActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    private void removeJobFromSupplierData(Job job) {
        List<Job> jobs = supplier.getJobs();
        jobs.remove(job);
        supplier.setJobs(jobs);
    }

    private void showErrorMessage() {
        new AlertDialog
                .Builder(this)
                .setTitle("Acción falló")
                .setMessage("La operación no pudo ser realizada")
                .setPositiveButton("OK", (dialog, whichButton) -> finish())
                .setCancelable(false)
                .show();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        operationFinishedSuccessfully();
    }

}
