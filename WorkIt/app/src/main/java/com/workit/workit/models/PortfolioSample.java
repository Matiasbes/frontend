package com.workit.workit.models;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Matías on 5/31/2017.
 */

public class PortfolioSample implements Serializable{
    private int id;
    private String title;
    private String description;
    private Date date;
    private transient File photo;

    public PortfolioSample() {
        this.photo = null;
    }

    public PortfolioSample(final int id,
                           final String title,
                           final String description,
                           final Date date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.photo = null;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public File getPhoto() {
        return this.photo;
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof PortfolioSample) {
            PortfolioSample object = (PortfolioSample) anObject;
            return this.getId() == object.getId();
        }
        return false;
    }
}
