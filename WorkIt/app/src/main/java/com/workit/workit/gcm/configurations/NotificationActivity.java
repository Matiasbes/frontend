package com.workit.workit.gcm.configurations;

import android.support.v7.app.AppCompatActivity;

import rx.Observable;
import rx_gcm.GcmReceiverUIForeground;
import rx_gcm.Message;

/**
 * Created by Matías on 6/15/2017.
 */

public class NotificationActivity extends AppCompatActivity implements GcmReceiverUIForeground {

    @Override
    public void onTargetNotification(Observable<Message> oMessage) {
        oMessage.subscribe(message -> WorkItGcmNotificationScheduler.buildAndShowNotification(this, message));
    }

    @Override
    public void onMismatchTargetNotification(Observable<Message> oMessage) {
    }

    @Override
    public boolean matchesTarget(String key) {
        return true;
    }
}

