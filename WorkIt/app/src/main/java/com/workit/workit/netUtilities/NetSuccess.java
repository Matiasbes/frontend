package com.workit.workit.netUtilities;

/**
 * Created by Matías on 5/18/2017.
 */

public class NetSuccess {

    private Boolean success;

    public NetSuccess(final Boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() { return success; }
}
