package com.workit.workit.models;

import java.io.Serializable;

/**
 * Created by Matías on 6/13/2017.
 */

public class UserRating implements Serializable{

    private int rating;

    public UserRating() {

    }

    public UserRating(final int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return this.rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
