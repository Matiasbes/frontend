package com.workit.workit.netUtilities;

/**
 * Created by Matías on 5/18/2017.
 */

public class NetException extends Exception {
    public NetException (String message) {
        super(message);
    }

    public NetException (NetException error) {

        super(error.getMessage());
    }
    public String getBodyMessage(String message){
        String[] separated = message.split(":");
        separated[1]=separated[1].replace("\"","");
        separated[1]=separated[1].replace("}","");
        return separated[1].trim();
    }
}
