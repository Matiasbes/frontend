package com.workit.workit.activities.profile.clients.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.DegreeListAdapter;
import com.workit.workit.fragments.main.EmptyViewFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Degree;
import com.workit.workit.models.Supplier;
import com.workit.workit.services.DegreeService;
import com.workit.workit.utilities.Constants;

public class DegreesActivity extends NotificationActivity implements DegreeListAdapter.DegreesDataProvider {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.empty_state_view) LinearLayout emptyStateView;

    private DegreeListAdapter adapter;

    private Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_degrees);
        ButterKnife.bind(this);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(
                R.id.empty_state_view,
                EmptyViewFragment.newInstance("Sin datos", "No se ha agregado ninguna certificación de estudio"),
                "emptyView").commit();

        updateInterface();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Educación");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> operationFinishedSuccessfully());
        floatingActionButton.setOnClickListener(view -> addDegree());

        loadDegrees();
    }

    private void updateInterface() {
        if (supplier.getDegrees() == null || supplier.getDegrees().isEmpty()) {
            listItems.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            listItems.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
        }
    }

    private void addDegree() {
        Intent intent = new Intent(this, DegreesControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        startActivityForResult(intent, Constants.ADD_ELEMENT_RETURN_CODE);
    }

    private void loadDegrees() {
        adapter = new DegreeListAdapter(this, this);
        listItems.setAdapter(adapter);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        Degree degree = (Degree)data.getExtras().getSerializable(Constants.DEGREE_INTENT_KEY);

        if (requestCode == Constants.ADD_ELEMENT_RETURN_CODE)
            supplier.addDegree(degree);
        else {
            List<Degree> degrees = supplier.getDegrees();
            degrees.set(degrees.indexOf(degree), degree);
            supplier.setDegrees(degrees);
        }

        updateInterface();
        adapter.notifyDataSetChanged();
        listItems.invalidate();
    }

    public List<Degree> getDegrees() {
        return supplier.getDegrees();
    }

    public void editDegree(Degree degree) {
        Intent intent = new Intent(this, DegreesControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        intent.putExtra(Constants.DEGREE_INTENT_KEY, degree);
        this.startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    public void removeDegree(Degree degree) {
        new AlertDialog
                .Builder(this)
                .setTitle("Borrar historial laboral")
                .setMessage("¿Estás seguro de borrar este item?")
                .setPositiveButton("Si", (dialog, whichButton) -> performDegreeRemoval(degree))
                .setNegativeButton("No", null)
                .show();
    }

    private void performDegreeRemoval(Degree degree) {
        ProgressDialog progressDialog = presentProgressDialog("Procesando...");
        new DegreeService(this)
                .removeDegree(degree.getId())
                .subscribe(
                        message -> {
                            progressDialog.hide();
                            progressDialog.dismiss();

                            removeDegreeFromSupplierData(degree);
                            adapter.notifyDataSetChanged();
                            listItems.invalidate();
                            updateInterface();
                        },
                        error -> showErrorMessage());
    }

    private ProgressDialog presentProgressDialog(String message) {
        ProgressDialog progressDialog = new ProgressDialog(DegreesActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    private void removeDegreeFromSupplierData(Degree degree) {
        List<Degree> degrees = supplier.getDegrees();
        degrees.remove(degree);
        supplier.setDegrees(degrees);
    }

    private void showErrorMessage() {
        new AlertDialog
                .Builder(this)
                .setTitle("Acción falló")
                .setMessage("La operación no pudo ser realizada")
                .setPositiveButton("OK", (dialog, whichButton) -> finish())
                .setCancelable(false)
                .show();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        operationFinishedSuccessfully();
    }

}
