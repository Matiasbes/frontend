package com.workit.workit.activities.profile.clients.suppliers;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Degree;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.DegreeService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DegreesControllerActivity extends NotificationActivity {

    private Supplier supplier;
    private Degree degree;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.actionButton) Button button;

    @BindView(R.id.institution) EditText institutionEditText;
    @BindView(R.id.start_date) EditText startDateEditText;
    @BindView(R.id.end_date) EditText endDateEditText;
    @BindView(R.id.title) EditText titleEditText;
    @BindView(R.id.description) EditText descriptionEditText;

    private Date startDate;
    private Date endDate;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_degrees_controller);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.SUPPLIER_INTENT_KEY);
        degree = (Degree) getIntent().getSerializableExtra(Constants.DEGREE_INTENT_KEY);
        setupView();
    }

    private void setupView() {
        setupProgressDialog();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> cancelOperation());

        if (degree == null)
            setupViewToAddMode();
        else
            setupViewToModifyMode();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(DegreesControllerActivity.this);
    }

    private void cancelOperation() {
        progressDialog.dismiss();
        hideKeyboard();
        finish();
    }

    private void setupViewToAddMode() {
        getSupportActionBar().setTitle("Agregar certificado de estudio");
        button.setText("Agregar certificado de estudio");
        GregorianCalendar calendar = new GregorianCalendar();
        setupStartDate(calendar);
        setupEndDate(calendar);
    }

    private void setupStartDate(GregorianCalendar calendar) {
        startDateEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                startDate = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(startDate);
                startDateEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void setupEndDate(GregorianCalendar calendar) {
        endDateEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                endDate = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(endDate);
                endDateEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void setupViewToModifyMode() {
        getSupportActionBar().setTitle("Modificar certificado de estudio");
        button.setText("Modificar certificado de estudio");
        startDate = degree.getStartDate();
        endDate = degree.getEndDate();

        institutionEditText.setText(degree.getInstitution());
        startDateEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(startDate));
        endDateEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(endDate));;
        titleEditText.setText(degree.getTitle());
        descriptionEditText.setText(degree.getDescription());

        GregorianCalendar startDateCalendar = new GregorianCalendar();
        startDateCalendar.setTime(degree.getStartDate());
        setupStartDate(startDateCalendar);

        GregorianCalendar endDateCalendar = new GregorianCalendar();
        endDateCalendar.setTime(degree.getEndDate());
        setupEndDate(endDateCalendar);
    }

    public void performDegreeAction(View view) {
        String institution = getInstitutionText();
        Date finalStartDate = getStartDate();
        Date finalEndDate = getEndDate();
        String title = getTitleText();
        String description = descriptionEditText.getText().toString();
        if (institution != null && finalStartDate != null && finalEndDate != null && title != null)
            performNetworkAction(new Degree(0, institution, finalStartDate, finalEndDate, title, description));
    }

    private String getInstitutionText() {
        String enterprise = institutionEditText.getText().toString();
        if (enterprise.isEmpty()) {
            institutionEditText.setError("Este campo es obligatorio");
            return null;
        }
        return enterprise;
    }

    private Date getStartDate() {
        if (startDate == null)
            startDateEditText.setError("Este campo es obligatorio");
        return startDate;
    }

    private Date getEndDate() {
        if (endDate == null)
            endDateEditText.setError("Este campo es obligatorio");
        return endDate;
    }

    private String getTitleText() {
        String title = titleEditText.getText().toString();
        if (title.isEmpty()) {
            titleEditText.setError("Este campo es obligatorio");
            return null;
        }
        return title;
    }

    private void performNetworkAction(Degree degree) {
        presentProgressDialog("Procesando...");
        hideKeyboard();
        if (this.degree == null)
            addDegree(degree);
        else
            modifyDegree(degree);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void addDegree(Degree degree) {
        new SupplierService(this)
                .addDegree(supplier.getId(), degree)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void operationFinishedSuccessfully(Degree degree) {
        Intent intent = new Intent();
        intent.putExtra(Constants.DEGREE_INTENT_KEY, degree);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(DegreesControllerActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    private void modifyDegree(Degree degree) {
        new DegreeService(this)
                .modifyDegree(this.degree.getId(), degree)
                .subscribe(
                        theDegree -> {
                            this.degree = theDegree;
                            operationFinishedSuccessfully(theDegree);
                        },
                        this::handleError);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
