package com.workit.workit.activities.projects;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.workit.workit.R;
import com.workit.workit.models.ProjectLocation;
import com.workit.workit.models.Project;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.PermissionUtils;

public class ProjectsMapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMapLongClickListener{

    private Project project;
    private GoogleMap mMap;
    private boolean mPermissionDenied = false;
    private Marker marker;
    private FloatingActionButton fab;
    private ProjectLocation projectLocation;
    private boolean shouldEnableLocation;
    private boolean shouldOnlyUserLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_maps);

        shouldOnlyUserLocation = getIntent().getBooleanExtra("shouldOnlyUserLocation", false);

        if(shouldOnlyUserLocation){
            fab = (FloatingActionButton) this.findViewById(R.id.fab);
            fab.setOnClickListener(this::getSelectedLocation);
            fab.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(),R.drawable.ic_done_black_76));
        }
        else{
            project = (Project)getIntent().getSerializableExtra(Constants.PROJECT_INTENT_KEY);
            shouldEnableLocation = getIntent().getBooleanExtra("shouldEnableLocation", false);

            fab = (FloatingActionButton) this.findViewById(R.id.fab);
            fab.setOnClickListener(shouldEnableLocation? this::addLocation: this::closeMap);
            fab.setImageDrawable(shouldEnableLocation ?ContextCompat.getDrawable(this.getApplicationContext(),R.drawable.ic_done_black_76):ContextCompat.getDrawable(this.getApplicationContext(),R.drawable.ic_close_black_76));

        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(shouldOnlyUserLocation)
            Toast.makeText(this,"Seleccióne ubicación con la cual se hará el filtro", Toast.LENGTH_LONG);

        if(shouldEnableLocation || shouldOnlyUserLocation){
            mMap.setOnMapLongClickListener(this);
        }
        mMap.setOnMapClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        if(project != null && project.getProjectLocation() != null){
            LatLng projectLocation = new LatLng(project.getProjectLocation().getLatitude(),project.getProjectLocation().getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(projectLocation));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(projectLocation,14));
            if(marker != null)
                marker.setPosition(projectLocation);
            else{
                marker = mMap.addMarker(new MarkerOptions().position(projectLocation).title("Proyecto").draggable(true));
            }
        }
        enableMyLocation();
    }

    @Override
    public void onMapClick(LatLng point) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
    }

    @Override
    public void onMapLongClick(LatLng point) {
        if(marker != null)
            marker.setPosition(point);
        else{
            if(shouldOnlyUserLocation)
                marker = mMap.addMarker(new MarkerOptions().position(point).title("Ubicación").draggable(true));
            else
                marker = mMap.addMarker(new MarkerOptions().position(point).title("Proyecto").draggable(true));
            projectLocation = new ProjectLocation(point.latitude, point.longitude);
        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the projectLocation is missing.
            PermissionUtils.requestPermission(this, Constants.LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the projectLocation has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "Buscando ubicación", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    public void addLocation(View view){
        project.setProjectLocation(projectLocation);
        Intent intent = new Intent();
        intent.putExtra(Constants.PROJECT_INTENT_KEY,project);
        setResult(RESULT_OK, intent);
        this.finish();
    }

    public void getSelectedLocation(View view){
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_LOCATION, projectLocation);
        setResult(RESULT_OK, intent);
        this.finish();
    }

    public void closeMap(View view){
        this.finish();
    }
}
