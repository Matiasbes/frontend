package com.workit.workit.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.workit.workit.models.User;

/**
 * Created by Matías on 5/22/2017.
 */

public class AuthenticationHelper {
    public static void removeToken(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.remove(Constants.USER_TYPE_PREFERENCE_KEY);
        editor.remove(Constants.TOKEN_PREFERENCES_KEY);
        editor.commit();
    }

    public static void saveLoginInformation(Activity activity, User user) {
        SharedPreferences.Editor editor = activity.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.TOKEN_PREFERENCES_KEY, user.getToken());
        editor.putInt(Constants.USER_TYPE_PREFERENCE_KEY, user.getType().getValue());
        editor.putInt(Constants.USER_ID_PREFERENCE_KEY, user.getId());
        editor.commit();
    }

}
