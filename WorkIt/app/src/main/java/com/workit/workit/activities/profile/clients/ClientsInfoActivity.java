package com.workit.workit.activities.profile.clients;

import android.app.ProgressDialog;
import android.content.Intent;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Client;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.netUtilities.NetSuccess;
import com.workit.workit.services.ClientService;
import com.workit.workit.utilities.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientsInfoActivity extends NotificationActivity {


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.name) EditText nameEditText;
    @BindView(R.id.phone) EditText phoneEditText;

    private Client client;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients_info);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Modificar inforamción");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> finish());

        setupProgressDialog();

        client = (Client) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        setupClientInfo();

    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(this);
    }

    private void setupClientInfo() {
        nameEditText.setText(client.getName());
        phoneEditText.setText(client.getPhone());
    }

    public void performClientEditAction(View view) {
        String name = getName();
        String phone = getPhone();

        if (name != null || phone != null) {
            client.setName(name);
            client.setPhone(phone);
            saveInformation();
        }
    }

    private String getName() {
        String name = nameEditText.getText().toString();
        if (name.isEmpty()) {
            nameEditText.setError("Este campo es obligatorio");
            return null;
        }
        return name;
    }

    private String getPhone() {
        String phone = phoneEditText.getText().toString();
        if (phone.isEmpty()) {
            phoneEditText.setError("Este campo es obligatorio");
            return null;
        }
        return phone;
    }

    private void saveInformation() {
        presentProgressDialog("Procesando...");
        new ClientService(this)
                .modifyClient(client.getId(), client)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully(NetSuccess networkSuccess) {
        Intent intent = new Intent();
        client.setName(nameEditText.getText().toString());
        client.setPhone(phoneEditText.getText().toString());
        intent.putExtra(Constants.USER_INTENT_KEY, client);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }
}
