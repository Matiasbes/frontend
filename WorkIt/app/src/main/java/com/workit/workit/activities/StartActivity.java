package com.workit.workit.activities;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserType;
import com.workit.workit.services.ClientService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;


public class StartActivity extends NotificationActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

        if (isTokenStored())
            attemptAutoLogin();
        else
            showLoginActivity();
    }

    private boolean isTokenStored() {
        return sharedPreferences.contains(Constants.TOKEN_PREFERENCES_KEY);
    }

    private void attemptAutoLogin() {
        UserType type = UserType.values()[sharedPreferences.getInt(Constants.USER_TYPE_PREFERENCE_KEY, 0)];
        if (type == UserType.CLIENT)
            authenticateClient();
        else
            authenticateSupplier();

    }

    private void authenticateClient() {
        new ClientService(this)
                .getClient(sharedPreferences.getInt(Constants.USER_ID_PREFERENCE_KEY, 0))
                .subscribe(client -> {
                            client.setType(UserType.CLIENT.getValue());
                            userLoggedIn(client);
                        },
                        error -> showLoginActivity());
    }

    private void authenticateSupplier() {
        new SupplierService(this)
                .getSupplier(sharedPreferences.getInt(Constants.USER_ID_PREFERENCE_KEY, 0))
                .subscribe(supplier -> {
                            supplier.setType(UserType.SUPPLIER.getValue());
                            userLoggedIn(supplier);
                        },
                        error -> showLoginActivity());
    }

    private void showLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    private void userLoggedIn(User user) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.USER_INTENT_KEY, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
