package com.workit.workit.models;

import java.io.Serializable;

/**
 * Created by Matías on 6/20/2017.
 */

public class ProjectLocation implements Serializable {

    private double latitude;
    private double longitude;

    public ProjectLocation(){}

    public ProjectLocation(final double latitude,final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof ProjectLocation) {
            ProjectLocation object = (ProjectLocation) anObject;
            return this.getLatitude() == object.getLatitude() && this.getLongitude() == object.getLongitude();
        }
        return false;
    }
}
