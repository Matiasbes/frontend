package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.Client;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/22/2017.
 */

public class ClientService extends NetService<ClientService.ClientServiceApi> {
    public ClientService(Activity activity) {
        super(activity, ClientServiceApi.class);
    }

    public interface ClientServiceApi {

        @GET("clients/{id}")
        Observable<Client> getClient(@Path("id") int id);

        @PUT("clients/{id}")
        Observable<NetSuccess> modifyClient(@Path("id") int id, @Body Client client);

    }

    public Observable<Client> getClient(int id) {
        Observable<Client> client = buildService()
                                        .getClient(id)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .onErrorResumeNext(this::networkErrorEmitter);
        return client;
    }

    public Observable<NetSuccess> modifyClient(int id, Client client) {
        Observable<NetSuccess> netSuccess = buildService()
                                                .modifyClient(id,client)
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .onErrorResumeNext(this::networkErrorEmitter);
        return netSuccess;
    }
}
