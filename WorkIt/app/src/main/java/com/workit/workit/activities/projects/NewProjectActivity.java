package com.workit.workit.activities.projects;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Client;
import com.workit.workit.models.Project;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.ProjectService;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.PhotoPickerAlertDialog;

public class NewProjectActivity extends NotificationActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.title) EditText titleEditText;
    @BindView(R.id.description) EditText descriptionEditText;
    @BindView(R.id.budget) EditText budgetEditText;
    @BindView(R.id.deadline) EditText deadlineEditText;
    @BindView(R.id.project_location_textview) TextView projectLocationTextView;

    private Client client;
    private Project project;
    private Date deadline;

    private File photo;
    private boolean photoAdded;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        photo = null;
        photoAdded = false;

        getSupportActionBar().setTitle("Crear proyecto");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> finish());
        setupProgressDialog();
        project = new Project();
        client = (Client) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);

        GregorianCalendar calendar = new GregorianCalendar();
        setupDeadline(calendar);
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(NewProjectActivity.this);
    }

    private void setupDeadline(GregorianCalendar calendar) {
        deadlineEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                deadline = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(deadline);
                deadlineEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void performAddProject(View view) {
        String title = titleEditText.getText().toString();
        if (title.isEmpty()) {
            titleEditText.setError("Debes ingresar el título del proyecto");
            return;
        }

        String description = descriptionEditText.getText().toString();
        if (description.isEmpty()) {
            descriptionEditText.setError("Debes ingresar una descripción");
            return;
        }

        float budget = getBudget();
        if (budget <= 0) {
            budgetEditText.setError("El presupuesto del proyecto debe ser mayor a cero");
            return;
        }

        if (deadline == null) {
            deadlineEditText.setError("La fecha ingresada es inválida");
            return;
        }

        if(project.getProjectLocation() == null){
            projectLocationTextView.setText("El proyecto debe tener ubicación");
            projectLocationTextView.setTextColor(Color.RED);
            return;
        }

        project.setTitle(title);
        project.setDescription(description);
        project.setBudget(budget);
        project.setDeadline(deadline);
        project.setClient(client);

        saveInformation(project);
    }

    private void loadPhoto() {
        Uri uri = getPhotoUri();
        if (uri == null)
            return;
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.image);
        draweeView.setImageURI(uri);
    }
    private Uri getPhotoUri() {
        if (photo != null)
            return Uri.fromFile(photo);
        if (project.getPhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "projects/" + project.getId() + "/image");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(project.getPhoto());
    }

    private Float getBudget() {
        try {
            return Float.parseFloat(budgetEditText.getText().toString());
        } catch (Exception e) {
            return -1.0f;
        }
    }

    private void saveInformation(Project project) {
        hideKeyboard();
        presentProgressDialog("Procesando...");
        new ProjectService(this)
                .addProject(project)
                .subscribe(
                        theProject -> {
                            this.project = theProject;
                            if(photoAdded)
                                savePhoto();
                            else
                              operationFinishedSuccessfully(theProject);
                        }, this::handleError);
    }

    private void savePhoto() {
        project.setPhoto(photo);
        new ProjectService(this)
                .changePhoto(project.getId(), photo)
                .subscribe(
                        success -> operationFinishedSuccessfully(project),
                        this::handleError);
    }
    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully(Project project) {
        Intent intent = new Intent();
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        setResult(RESULT_OK, intent);

        finish();
        progressDialog.hide();
        progressDialog.dismiss();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException )error;
        new AlertDialog.Builder(NewProjectActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }
    public void addProjectSampleImage(View view) {
        PhotoPickerAlertDialog.showPicker(this, "Seleccionar imagen a usar");
    }

    public void addLocation(View view){
        Intent intent = new Intent(view.getContext(),ProjectsMapsActivity.class);
        intent.putExtra(Constants.PROJECT_INTENT_KEY, project);
        intent.putExtra("shouldEnableLocation", true);
        ((Activity)view.getContext()).startActivityForResult(intent, Constants.MAP_ACTION_RETURN_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE ||
                requestCode == Constants.GALLERY_ACTION_RETURN_CODE) {

            if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE)
                photo = PhotoPickerAlertDialog.imageFromCamera(this, data);
            else
                photo = PhotoPickerAlertDialog.imageFromGallery(this, data);
            photoAdded = true;
            loadPhoto();
        }
        if(requestCode == Constants.MAP_ACTION_RETURN_CODE){
            Project projectLocation = (Project)data.getSerializableExtra(Constants.PROJECT_INTENT_KEY);
            project.setProjectLocation(projectLocation.getProjectLocation());
            projectLocationTextView.setText("Ubicación del proyecto");
            projectLocationTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }
}
