package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.SimpleDateFormat;
import java.util.List;
import com.workit.workit.R;
import com.workit.workit.models.PortfolioSample;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.Helpers;

/**
 * Created by Matías on 6/4/2017.
 */

public class PortfolioListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Activity activity;

    private boolean allowsDataEdition;

    private PortfolioDataProvider dataProvider;

    public PortfolioListAdapter(Activity activity, PortfolioDataProvider dataProvider) {
        this(activity, dataProvider, true);
    }

    public PortfolioListAdapter(Activity activity, PortfolioDataProvider dataProvider, boolean allowsDataEdition) {
        this.dataProvider = dataProvider;
        this.activity = activity;
        this.allowsDataEdition = allowsDataEdition;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getPortfolioSamples().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getPortfolioSamples().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getPortfolioSamples().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.portfolio_card, null);
        PortfolioSample portfolioSample = dataProvider.getPortfolioSamples().get(position);

        loadImage(view, portfolioSample);
        ((TextView) view.findViewById(R.id.title)).setText(portfolioSample.getTitle());
        ((TextView) view.findViewById(R.id.description)).setText(portfolioSample.getDescription());
        ((TextView) view.findViewById(R.id.date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(portfolioSample.getDate()));

        SimpleDraweeView editButton = (SimpleDraweeView) view.findViewById(R.id.edit_button);
        SimpleDraweeView deleteButton = (SimpleDraweeView) view.findViewById(R.id.delete_button);

        if (allowsDataEdition) {
            editButton.setOnClickListener(theView -> dataProvider.editPortfolioSample(portfolioSample));
            deleteButton.setOnClickListener(theView -> dataProvider.removePortfolioSample(portfolioSample));
        } else {
            view.findViewById(R.id.options_container).setVisibility(View.GONE);
            view.findViewById(R.id.separator_view).setVisibility(View.GONE);
        }

        return view;
    }

    private void loadImage(View superview, PortfolioSample portfolioSample) {
        Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "portfolio/" + portfolioSample.getId() + "/image");
        SimpleDraweeView draweeView = (SimpleDraweeView) superview.findViewById(R.id.image);
        draweeView.setImageURI(uri);
    }

    public interface PortfolioDataProvider {

        List<PortfolioSample> getPortfolioSamples();
        void editPortfolioSample(PortfolioSample portfolioSample);
        void removePortfolioSample(PortfolioSample portfolioSample);

    }


}
