package com.workit.workit.models;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Matías on 5/31/2017.
 */

public class Project implements Serializable{
    private int id;
    private String title;
    private String description;
    private ProjectStatus status;
    private float budget;
    private Date deadline;
    private User client;
    private List<User> suppliers;
    private List<ProjectRating> ratings;
    private transient File photo;
    private ProjectLocation location;

    public Project() {
        suppliers = new ArrayList<>();
        ratings = new ArrayList<>();
        this.photo = null;
    }

    public Project(final int id,
                   final String title,
                   final String description,
                   final String status,
                   final float budget,
                   final Date deadline,
                   final User client,
                   final List<User> suppliers,
                   final List<ProjectRating> ratings,
                   final ProjectLocation location) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = ProjectStatus.get(status);
        this.budget = budget;
        this.deadline = deadline;
        this.client = client;
        this.suppliers = suppliers;
        this.ratings = ratings;
        this.photo = null;
        this.location = location;
    }
    public List<User> getSuppliers() {
        return this.suppliers;
    }

    public void setSuppliers(List<User> suppliers) {
        this.suppliers = suppliers;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectStatus getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = ProjectStatus.get(status);
    }

    public float getBudget() {
        return this.budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public Date getDeadline() {
        return this.deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public User getClient() {
        return this.client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public List<ProjectRating> getRatings() {
        return this.ratings;
    }

    public void setRatings(List<ProjectRating> ratings) {
        this.ratings = ratings;
    }

    public File getPhoto() {
        return this.photo;
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    public ProjectLocation getProjectLocation() {
        return location;
    }

    public void setProjectLocation(ProjectLocation location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof Project) {
            Project object = (Project) anObject;
            return this.getId() == object.getId();
        }
        return false;
    }
}
