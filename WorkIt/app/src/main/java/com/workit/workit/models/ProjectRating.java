package com.workit.workit.models;

import java.io.Serializable;

/**
 * Created by Matías on 5/31/2017.
 */

public class ProjectRating implements Serializable{
    private Supplier supplier;
    private int clientRate;
    private int supplierRate;

    public ProjectRating() {

    }

    public ProjectRating(final Supplier supplier,
                         final int clientRate,
                         final int supplierRate) {
        this.supplier = supplier;
        this.clientRate = clientRate;
        this.supplierRate = supplierRate;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public int getClientRate() {
        return clientRate;
    }

    public void setClientRate(int clientRate) {
        this.clientRate = clientRate;
    }

    public int getSupplierRate() {
        return supplierRate;
    }

    public void setSupplierRate(int supplierRate) {
        this.supplierRate = supplierRate;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof ProjectRating) {
            ProjectRating object = (ProjectRating) anObject;
            return this.getSupplier().getId() == object.getSupplier().getId();
        }
        return false;
    }
}
