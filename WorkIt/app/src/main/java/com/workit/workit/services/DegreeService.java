package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.Degree;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/26/2017.
 */

public class DegreeService extends NetService<DegreeService.DegreeServiceApi> {

    public DegreeService(Activity activity) {
        super(activity, DegreeServiceApi.class);
    }

    public interface DegreeServiceApi {

        @GET("degrees/{id}")
        Observable<Degree> getDegree(@Path("id") int id);

        @PUT("degrees/{id}")
        Observable<Degree> modifyDegree(@Path("id") int id, @Body Degree degree);

        @DELETE("degrees/{id}")
        Observable<NetSuccess> removeDegree(@Path("id") int id);

    }

    public Observable<Degree> getDegree(int id) {
        Observable<Degree> degree = buildService()
                .getDegree(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return degree;
    }

    public Observable<Degree> modifyDegree(int id, Degree dataDegree) {
        Observable<Degree> degree = buildService()
                .modifyDegree(id, dataDegree)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return degree;
    }

    public Observable<NetSuccess> removeDegree(int id) {
        Observable<NetSuccess> success = buildService()
                .removeDegree(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        return success;
    }

}