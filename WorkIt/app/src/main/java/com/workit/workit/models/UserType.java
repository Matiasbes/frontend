package com.workit.workit.models;

import com.google.gson.annotations.SerializedName;
/**
 * Created by Matías on 5/16/2017.
 */

public enum UserType {
    @SerializedName("0")
    CLIENT(0),

    @SerializedName("1")
    SUPPLIER(1);

    private final int value;

    UserType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
