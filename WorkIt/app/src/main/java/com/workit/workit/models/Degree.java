package com.workit.workit.models;

import java.io.Serializable;
import java.util.Date;
/**
 * Created by Matías on 5/22/2017.
 */

public class Degree implements Serializable{
    private int id;
    private String institution;
    private Date startDate;
    private Date endDate;
    private String title;
    private String description;

    public Degree() {

    }

    public Degree(final int id,
                  final String institution,
                  final Date startDate,
                  final Date endDate,
                  final String title,
                  final String description) {
        this.id = id;
        this.institution = institution;
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof Degree) {
            Degree object = (Degree) anObject;
            return this.getId() == object.getId();
        }
        return false;
    }
}
