package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.models.Skill;

import java.util.List;

/**
 * Created by Matías on 5/29/2017.
 */

public class SkillListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Activity activity;

    private SkillsDataProvider dataProvider;

    public SkillListAdapter(Activity activity, SkillsDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataProvider.getSkills().size();
    }

    @Override
    public Object getItem(int position) {
        return dataProvider.getSkills().get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataProvider.getSkills().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.skill_card, null);
        Skill skill = dataProvider.getSkills().get(position);

        ((TextView) view.findViewById(R.id.skill)).setText(skill.getTitle());

        view.findViewById(R.id.edit_button)
                .setOnClickListener(theView -> dataProvider.editSkill(skill));
        view.findViewById(R.id.delete_button)
                .setOnClickListener(theView -> dataProvider.removeSkill(skill));

        return view;
    }

    public interface SkillsDataProvider {

        List<Skill> getSkills();
        void editSkill(Skill skill);
        void removeSkill(Skill skill);

    }

}

