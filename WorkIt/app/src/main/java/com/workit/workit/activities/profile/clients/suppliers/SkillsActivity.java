package com.workit.workit.activities.profile.clients.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.SkillListAdapter;
import com.workit.workit.fragments.main.EmptyViewFragment;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Skill;
import com.workit.workit.models.Supplier;
import com.workit.workit.services.SkillService;
import com.workit.workit.utilities.Constants;

public class SkillsActivity extends NotificationActivity implements SkillListAdapter.SkillsDataProvider {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.list_items) ListView listItems;
    @BindView(R.id.empty_state_view) LinearLayout emptyStateView;

    private SkillListAdapter adapter;

    private Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills);
        ButterKnife.bind(this);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(
                R.id.empty_state_view,
                EmptyViewFragment.newInstance("Sin datos", "No se ha agregado ningún skill"),
                "emptyView").commit();

        updateInterface();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Skills");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> operationFinishedSuccessfully());

        floatingActionButton.setOnClickListener(view -> addSkill());

        loadSkills();
    }

    private void updateInterface() {
        if (supplier.getSkills() == null || supplier.getSkills().isEmpty()) {
            listItems.setVisibility(View.GONE);
            emptyStateView.setVisibility(View.VISIBLE);
        } else {
            listItems.setVisibility(View.VISIBLE);
            emptyStateView.setVisibility(View.GONE);
        }
    }

    private void addSkill() {
        Intent intent = new Intent(this, SkillsControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        startActivityForResult(intent, Constants.ADD_ELEMENT_RETURN_CODE);
    }

    private void loadSkills() {
        adapter = new SkillListAdapter(this, this);
        listItems.setAdapter(adapter);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        Skill skill = (Skill)data.getExtras().getSerializable(Constants.SKILL_INTENT_KEY);

        if (requestCode == Constants.ADD_ELEMENT_RETURN_CODE)
            supplier.addSkill(skill);
        else {
            List<Skill> skills = supplier.getSkills();
            skills.set(skills.indexOf(skill), skill);
            supplier.setSkills(skills);
        }

        updateInterface();
        adapter.notifyDataSetChanged();
        listItems.invalidate();
    }

    public List<Skill> getSkills() {
        return supplier.getSkills();
    }

    public void editSkill(Skill skill) {
        Intent intent = new Intent(this, SkillsControllerActivity.class);
        intent.putExtra(Constants.SUPPLIER_INTENT_KEY, supplier);
        intent.putExtra(Constants.SKILL_INTENT_KEY, skill);
        this.startActivityForResult(intent, Constants.EDIT_ELEMENT_RETURN_CODE);
    }

    public void removeSkill(Skill skill) {
        new AlertDialog
                .Builder(this)
                .setTitle("Borrar skill")
                .setMessage("¿Estás seguro de borrar este skill?")
                .setPositiveButton("Si", (dialog, whichButton) -> performSkillRemoval(skill))
                .setNegativeButton("No", null)
                .show();
    }

    private void performSkillRemoval(Skill skill) {
        ProgressDialog progressDialog = presentProgressDialog("Procesando...");
        new SkillService(this)
                .removeSkill(skill.getId())
                .subscribe(
                        message -> {
                            progressDialog.hide();
                            progressDialog.dismiss();

                            removeSkillFromSupplierData(skill);
                            updateInterface();
                            adapter.notifyDataSetChanged();
                            listItems.invalidate();
                        },
                        error -> showErrorMessage());
    }

    private ProgressDialog presentProgressDialog(String message) {
        ProgressDialog progressDialog = new ProgressDialog(SkillsActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    private void removeSkillFromSupplierData(Skill skill) {
        List<Skill> skills = supplier.getSkills();
        skills.remove(skill);
        supplier.setSkills(skills);
    }

    private void showErrorMessage() {
        new AlertDialog
                .Builder(this)
                .setTitle("Acción falló")
                .setMessage("La operación no pudo ser realizada")
                .setPositiveButton("OK", (dialog, whichButton) -> finish())
                .setCancelable(false)
                .show();
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        operationFinishedSuccessfully();
    }

}
