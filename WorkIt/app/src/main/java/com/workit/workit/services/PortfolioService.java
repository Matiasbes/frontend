package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.PortfolioSample;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 6/13/2017.
 */

public class PortfolioService extends NetService<PortfolioService.PortfolioServiceApi> {

    public PortfolioService(Activity activity) {
        super(activity, PortfolioServiceApi.class);
    }

    public interface PortfolioServiceApi {

        @GET("portfolio/{id}")
        Observable<PortfolioSample> getPortfolioSample(@Path("id") int id);

        @PUT("portfolio/{id}")
        Observable<PortfolioSample> modifyPortfolioSample(@Path("id") int id, @Body PortfolioSample portfolioSample);

        @DELETE("portfolio/{id}")
        Observable<NetSuccess> removePortfolioSample(@Path("id") int id);

        @Multipart
        @PUT("portfolio/{id}/image")
        Observable<NetSuccess> changePhoto(@Path("id") int id, @Part("images") RequestBody file);

    }

    public Observable<PortfolioSample> getPortfolioSample(int id) {
        return buildService()
                .getPortfolioSample(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<PortfolioSample> modifyPortfolioSample(int id, PortfolioSample portfolioSample) {
        return buildService()
                .modifyPortfolioSample(id, portfolioSample)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<NetSuccess> removePortfolioSample(int id) {
        return buildService()
                .removePortfolioSample(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<NetSuccess> changePhoto(@Path("id") int id, File file) {
        RequestBody body = RequestBody.create(MediaType.parse("images/*"), file);
        return buildService()
                .changePhoto(id, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }
}