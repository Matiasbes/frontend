package com.workit.workit.netUtilities;

import java.util.HashMap;

/**
 * Created by Matías on 5/16/2017.
 */

public class NetHashMap extends HashMap<String, Object> {
    public NetHashMap append(String key, Object value){
        put(key,value);
        return  this;
    }
    public NetHashMap(){};
}
