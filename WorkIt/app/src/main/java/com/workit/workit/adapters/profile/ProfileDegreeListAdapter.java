package com.workit.workit.adapters.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.workit.workit.R;
import com.workit.workit.models.Degree;
import com.workit.workit.models.Supplier;

import java.text.SimpleDateFormat;

public class ProfileDegreeListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private Supplier supplier;

    public ProfileDegreeListAdapter(Activity activity, Supplier supplier) {
        this.supplier = supplier;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return supplier.getDegrees().size();
    }

    @Override
    public Object getItem(int position) {
        return supplier.getDegrees().get(position);
    }

    @Override
    public long getItemId(int position) {
        return supplier.getDegrees().get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = (View) inflater.inflate(R.layout.degree_profile_listing, null);
        Degree degree = supplier.getDegrees().get(position);

        ((TextView) view.findViewById(R.id.institution)).setText(degree.getInstitution());
        ((TextView) view.findViewById(R.id.start_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(degree.getStartDate()));
        ((TextView) view.findViewById(R.id.end_date)).setText(new SimpleDateFormat("MMMM dd, yyyy").format(degree.getEndDate()));
        ((TextView) view.findViewById(R.id.title)).setText(degree.getTitle());

        TextView description = (TextView) view.findViewById(R.id.description);
        if (degree.getDescription() != null && !degree.getDescription().isEmpty())
            description.setText(degree.getDescription());
        else {
            description.setVisibility(View.GONE);
            view.findViewById(R.id.description_label).setVisibility(View.GONE);
        }

        return view;
    }

}
