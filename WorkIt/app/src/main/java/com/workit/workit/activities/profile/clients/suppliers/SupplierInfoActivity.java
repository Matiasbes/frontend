package com.workit.workit.activities.profile.clients.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.netUtilities.NetSuccess;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SupplierInfoActivity extends NotificationActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.name) EditText nameEditText;
    @BindView(R.id.phone) EditText phoneEditText;
    @BindView(R.id.overview) EditText overviewEditText;
    @BindView(R.id.rate) EditText rateEditText;

    private Supplier supplier;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_info);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Modificar información");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> finish());
        setupProgressDialog();

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        setupView();
    }

    private void setupView() {
        nameEditText.setText(supplier.getName());
        phoneEditText.setText(supplier.getPhone());
        overviewEditText.setText(supplier.getOverview());
        rateEditText.setText(Float.toString(supplier.getRate()));
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(this);
    }

    public void performSupplierEditAction(View view) {
        String name = getName();
        String phone = getPhone();
        String overview = getOverview();
        float rate = getRate();

        if (!name.isEmpty() && !phone.isEmpty() && !overview.isEmpty()) {
            supplier.setName(name);
            supplier.setPhone(phone);
            supplier.setOverview(overview);
            supplier.setRate(rate);
            saveInformation();
        }
    }

    private String getName() {
        String name = nameEditText.getText().toString();
        if (name.isEmpty())
            nameEditText.setError("Este campo es obligatorio");
        return name;
    }

    private String getPhone() {
        String phone = phoneEditText.getText().toString();
        if (phone.isEmpty())
            phoneEditText.setError("Este campo es obligatorio");
        return phone;
    }

    private String getOverview() {
        String overview = overviewEditText.getText().toString();
        if (overview.isEmpty())
            overviewEditText.setError("Este campo es obligatorio");
        return overview;
    }

    private float getRate() {
        try {
            float rate = Float.parseFloat(rateEditText.getText().toString());
            if (rate <= 0 && !nameEditText.getText().toString().isEmpty())
                rateEditText.setError("La tarifa por hora debe ser mayor que cero");
            return rate;
        }
        catch (Exception e) {
            return 0;
        }
    }

    private void saveInformation() {
        presentProgressDialog("Procesando...");
        new SupplierService(this)
                .modifySupplier(supplier.getId(), supplier)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void operationFinishedSuccessfully(NetSuccess networkSuccess) {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);

        finish();
        progressDialog.hide();
        progressDialog.dismiss();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    @Override
    public void onBackPressed() {
        progressDialog.dismiss();
        finish();
    }
}
