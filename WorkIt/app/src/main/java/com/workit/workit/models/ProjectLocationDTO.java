package com.workit.workit.models;

import java.io.Serializable;

/**
 * Created by Matías on 6/21/2017.
 */

public class ProjectLocationDTO implements Serializable {

    private double latitude;
    private double longitude;
    private double distance;

    public ProjectLocationDTO(){}

    public ProjectLocationDTO(double latitude, double longitude, double distance) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof ProjectLocation) {
            ProjectLocation object = (ProjectLocation) anObject;
            return this.getLatitude() == object.getLatitude() && this.getLongitude() == object.getLongitude();
        }
        return false;
    }
}
