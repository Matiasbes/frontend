package com.workit.workit.activities.profile.clients.suppliers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.Skill;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.SkillService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;

public class SkillsControllerActivity extends NotificationActivity {

    private Supplier supplier;
    private Skill skill;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.actionButton) Button button;
    @BindView(R.id.skill) EditText skillEditText;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills_controller);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.SUPPLIER_INTENT_KEY);
        skill = (Skill) getIntent().getSerializableExtra(Constants.SKILL_INTENT_KEY);
        setupView();
    }

    private void setupView() {
        setupProgressDialog();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> cancelOperation());
        if (skill == null)
            setupViewToAddMode();
        else
            setupViewToModifyMode();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(SkillsControllerActivity.this);
    }

    private void cancelOperation() {
        progressDialog.dismiss();
        hideKeyboard();
        finish();
    }

    private void setupViewToAddMode() {
        getSupportActionBar().setTitle("Agregar skill");
        button.setText("Agregar skill");
    }

    private void setupViewToModifyMode() {
        getSupportActionBar().setTitle("Modificar skill");
        button.setText("Modificar skill");
        skillEditText.setText(skill.getTitle());
    }

    public void performAction(View view) {
        String title = skillEditText.getText().toString();
        if (title.isEmpty())
            skillEditText.setError("Este campo es obligatorio");
        else
            performNetworkAction(new Skill(0, title));
    }

    private void performNetworkAction(Skill skill) {
        presentProgressDialog("Procesando...");
        hideKeyboard();
        if (this.skill == null)
            addSkill(skill);
        else
            modifySkill(skill);
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void addSkill(Skill skill) {
        new SupplierService(this)
                .addSkill(supplier.getId(), skill)
                .subscribe(this::operationFinishedSuccessfully, this::handleError);
    }

    private void operationFinishedSuccessfully(Skill skill) {
        Intent intent = new Intent();
        intent.putExtra(Constants.SKILL_INTENT_KEY, skill);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog.Builder(SkillsControllerActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    private void modifySkill(Skill skill) {
        new SkillService(this)
                .modifySkill(this.skill.getId(), skill)
                .subscribe(
                        theSkill -> {
                            this.skill = theSkill;
                            operationFinishedSuccessfully(theSkill);
                        },
                        this::handleError);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
