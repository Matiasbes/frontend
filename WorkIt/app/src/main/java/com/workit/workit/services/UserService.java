package com.workit.workit.services;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.workit.workit.activities.LoginActivity;
import com.workit.workit.models.User;
import com.workit.workit.models.UserRating;
import com.workit.workit.netUtilities.NetError;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.netUtilities.NetHashMap;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.GsonUtil;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx_gcm.internal.RxGcm;

/**
 * Created by Matías on 5/16/2017.
 */

public class UserService extends NetService<UserService.UserServiceApi> {

    public UserService(Activity activity) {
        super(activity, UserServiceApi.class);
        this.activity = activity;
    }
    private Activity activity;
    public interface UserServiceApi {

        @POST("sessions")
        Observable<User> login(@Body NetHashMap body);

        @POST("users")
        Observable<User> signup(@Body NetHashMap user);

        @PUT("users/facebook/{id}")
        Observable<User> facebookLogin(@Path("id") String id, @Body NetHashMap body);

        @POST("users/facebook")
        Observable<User> facebookSignup(@Body User user);

        @PUT("users/{id}/password")
        Observable<User> changePassword(@Path("id") int id, @Body NetHashMap body);

        @Multipart
        @PUT("users/{id}/profileimage")
        Observable<NetSuccess> changeProfilePhoto(@Path("id") int id, @Part("images") RequestBody file);

        @GET("users/{id}/rating")
        Observable<UserRating> getRating(@Path("id") int id);

    }

    public Observable<User> login(String email, String password) {
        return RxGcm.Notifications
                .currentToken()
                .flatMap(token ->
                        buildService().login(new NetHashMap()
                                .append("email", email)
                                .append("password", password)
                                .append("deviceId", token))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<User> signup(User dataUser) {
        return RxGcm.Notifications
                .currentToken()
                .flatMap(token ->
                        buildService().signup(new NetHashMap()
                                .append("name", dataUser.getName())
                                .append("email", dataUser.getEmail())
                                .append("password", dataUser.getPassword())
                                .append("type", dataUser.getType())
                                .append("deviceId", token))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<User> facebookLogin(String facebookId, String email) {
        return buildService()
                .facebookLogin(facebookId, new NetHashMap()
                        .append("email", email))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<User> facebookSignup(User user) {
        return buildService()
                .facebookSignup(user)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }
    public Observable<User> changePassword(User user, String actualPassword, String newPassword) {
        Observable<User> modifyUser = buildService()
                                    .changePassword(user.getId(), new NetHashMap()
                                        .append("actualPassword", actualPassword)
                                        .append("newPassword", newPassword))
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorResumeNext(this::networkErrorEmitter);
        return modifyUser;
    }

    public Observable<NetSuccess> changeProfilePhoto(@Path("id") int id, File file) {
        RequestBody body = RequestBody.create(MediaType.parse("images/*"), file);
        return buildService()
                .changeProfilePhoto(id, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<UserRating> getRating(int id) {
        return buildService()
                .getRating(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }
}
