package com.workit.workit.models;

import java.io.Serializable;
import java.util.Date;
/**
 * Created by Matías on 5/22/2017.
 */

public class Job implements Serializable{
    private int id;
    private String enterprise;
    private Date startDate;
    private Date endDate;
    private String title;
    private String description;

    public Job() {

    }

    public Job(final int id,
               final String enterprise,
               final Date startDate,
               final Date endDate,
               final String title,
               final String description) {
        this.id = id;
        this.enterprise = enterprise;
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object anObject) {
        if (anObject instanceof Job) {
            Job object = (Job) anObject;
            return this.getId() == object.getId();
        }
        return false;
    }
}
