package com.workit.workit.netUtilities;

/**
 * Created by Matías on 5/16/2017.
 */

public class NetError {
    private String message;

    public NetError(final String message) {
        this.message = message;
    }

    public String getMessage() { return message; }
}
