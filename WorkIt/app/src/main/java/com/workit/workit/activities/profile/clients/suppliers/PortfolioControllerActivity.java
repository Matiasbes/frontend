package com.workit.workit.activities.profile.clients.suppliers;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.PortfolioSample;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetException;
import com.workit.workit.services.PortfolioService;
import com.workit.workit.services.SupplierService;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.PhotoPickerAlertDialog;

public class PortfolioControllerActivity extends NotificationActivity {

    private Supplier supplier;
    private PortfolioSample portfolioSample;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.actionButton) Button button;

    @BindView(R.id.title) EditText titleEditText;
    @BindView(R.id.description) EditText descriptionEditText;
    @BindView(R.id.date) EditText dateEditText;

    @BindView(R.id.scroll_view) ScrollView scrollView;

    private Date portfolioSampleDate;

    private File photo;
    private boolean photoAdded;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio_controller);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        photo = null;
        photoAdded = false;

        supplier = (Supplier) getIntent().getSerializableExtra(Constants.SUPPLIER_INTENT_KEY);
        portfolioSample = (PortfolioSample) getIntent().getSerializableExtra(Constants.PORTFOLIO_INTENT_KEY);
        setupView();
    }

    private void setupView() {
        setupProgressDialog();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> cancelOperation());
        scrollView.smoothScrollTo(0,0);

        if (portfolioSample == null)
            setupViewToAddMode();
        else
            setupViewToModifyMode();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(PortfolioControllerActivity.this);
    }

    private void cancelOperation() {
        progressDialog.dismiss();
        hideKeyboard();
        finish();
    }

    private void setupViewToAddMode() {
        getSupportActionBar().setTitle("Agregar item al portfolio");
        button.setText("Agregar item al portfolio");
        GregorianCalendar calendar = new GregorianCalendar();
        setupDate(calendar);
    }

    private void setupDate(GregorianCalendar calendar) {
        dateEditText.setOnClickListener(view -> {
            hideKeyboard();
            DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
                GregorianCalendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.set(Calendar.YEAR, year);
                selectedCalendar.set(Calendar.MONTH, month);
                selectedCalendar.set(Calendar.DAY_OF_MONTH, day);

                portfolioSampleDate = selectedCalendar.getTime();
                String date = new SimpleDateFormat("MMMM dd, yyyy").format(portfolioSampleDate);
                dateEditText.setText(date);
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        });
    }

    private void setupViewToModifyMode() {
        getSupportActionBar().setTitle("Modificar item del portfolio");
        button.setText("Modificar item del portfolio");
        portfolioSampleDate = portfolioSample.getDate();

        dateEditText.setText(new SimpleDateFormat("MMMM dd, yyyy").format(portfolioSampleDate));
        titleEditText.setText(portfolioSample.getTitle());
        descriptionEditText.setText(portfolioSample.getDescription());

        GregorianCalendar startDateCalendar = new GregorianCalendar();
        startDateCalendar.setTime(portfolioSample.getDate());
        setupDate(startDateCalendar);

        loadPhoto();
    }

    private void loadPhoto() {
        Uri uri = getPhotoUri();
        if (uri == null)
            return;
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.image);
        draweeView.setImageURI(uri);
    }

    private Uri getPhotoUri() {
        if (photo != null)
            return Uri.fromFile(photo);
        if (portfolioSample == null)
            return null;
        if (portfolioSample.getPhoto() == null) {
            Uri uri = Uri.parse(Constants.BASE_ENDPOINT + "portfolio/" + portfolioSample.getId() + "/image");
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainFileCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageFileCache().remove(new SimpleCacheKey(uri.toString()));
            return uri;
        }
        return Uri.fromFile(portfolioSample.getPhoto());
    }

    public void performPortfolioSampleAction(View view) {
        String title = getTitleText();
        String description = getDescriptionText();
        Date date = getDate();
        if (!title.isEmpty() && !description.isEmpty() && date != null )
            performNetworkAction(new PortfolioSample(0, title, description, date));
    }

    private Date getDate() {
        if (portfolioSampleDate == null)
            dateEditText.setError("Este campo es obligatorio");
        return portfolioSampleDate;
    }

    private String getTitleText() {
        String title = titleEditText.getText().toString();
        if (title.isEmpty())
            titleEditText.setError("Este campo es obligatorio");
        return title;
    }

    private String getDescriptionText() {
        String description = descriptionEditText.getText().toString();
        if (description.isEmpty())
            descriptionEditText.setError("Este campo es obligatorio");
        return description;
    }

    private void performNetworkAction(PortfolioSample portfolioSample) {
        presentProgressDialog("Procesando...");
        hideKeyboard();
        if (this.portfolioSample == null)
            addPortfolioSample(portfolioSample);
        else
            modifyPortfolioSample(portfolioSample);

    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    private void addPortfolioSample(PortfolioSample portfolioSample) {
        new SupplierService(this)
                .addPortfolioSample(supplier.getId(), portfolioSample)
                .subscribe(
                        thePortfolioSample -> {
                            this.portfolioSample = thePortfolioSample;
                            if (photoAdded)
                                savePhoto();
                            else
                                operationFinishedSuccessfully(thePortfolioSample);
                        },
                        this::handleError);
    }

    private void operationFinishedSuccessfully(PortfolioSample portfolioSample) {
        Intent intent = new Intent();
        intent.putExtra(Constants.PORTFOLIO_INTENT_KEY, portfolioSample);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    private void handleError(Throwable error) {
        NetException exception = (NetException)error;
        new AlertDialog
                .Builder(PortfolioControllerActivity.this)
                .setTitle("Operación falló")
                .setMessage(exception.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    private void modifyPortfolioSample(PortfolioSample portfolioSample) {
        new PortfolioService(this)
                .modifyPortfolioSample(this.portfolioSample.getId(), portfolioSample)
                .subscribe(
                        thePortfolioSample -> {
                            this.portfolioSample = thePortfolioSample;
                            if (photoAdded)
                                savePhoto();
                            else
                                operationFinishedSuccessfully(thePortfolioSample);
                        },
                        this::handleError);
    }

    public void addPortfolioSampleImage(View view) {
        PhotoPickerAlertDialog.showPicker(this, "Seleccionar imagen a usar");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE ||
                requestCode == Constants.GALLERY_ACTION_RETURN_CODE) {

            if (requestCode == Constants.CAMERA_ACTION_RETURN_CODE)
                photo = PhotoPickerAlertDialog.imageFromCamera(this, data);
            else
                photo = PhotoPickerAlertDialog.imageFromGallery(this, data);
            photoAdded = true;
            loadPhoto();
        }
    }

    private void savePhoto() {
        portfolioSample.setPhoto(photo);
        new PortfolioService(this)
                .changePhoto(portfolioSample.getId(), photo)
                .subscribe(
                        success -> operationFinishedSuccessfully(portfolioSample),
                        this::handleError);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }
}
