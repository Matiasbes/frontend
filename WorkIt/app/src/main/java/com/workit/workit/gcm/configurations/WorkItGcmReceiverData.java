package com.workit.workit.gcm.configurations;

import com.workit.workit.gcm.model.Notification;
import com.workit.workit.gcm.cache.Cache;

import rx.Observable;
import rx_gcm.GcmReceiverData;
import rx_gcm.Message;

/**
 * Created by Matías on 6/15/2017.
 */

public class WorkItGcmReceiverData implements GcmReceiverData {

    @Override
    public Observable<Message> onNotification(Observable<Message> oMessage) {
        return oMessage.doOnNext(message -> {
            Notification notification = WorkItGcmNotificationScheduler.parseNotificationFromGcmMessage(message);
            Cache.Pool.addNotification(notification);
        });
    }
}