package com.workit.workit.services;

import android.app.Activity;

import com.workit.workit.models.Degree;
import com.workit.workit.models.Job;
import com.workit.workit.models.PortfolioSample;
import com.workit.workit.models.Skill;
import com.workit.workit.models.Supplier;
import com.workit.workit.netUtilities.NetService;
import com.workit.workit.netUtilities.NetSuccess;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matías on 5/22/2017.
 */

public class SupplierService extends NetService<SupplierService.SupplierServiceApi> {
    public SupplierService(Activity activity) {
        super(activity, SupplierServiceApi.class);
    }

    public interface SupplierServiceApi {

        @GET("suppliers/{id}")
        Observable<Supplier> getSupplier(@Path("id") int id);

        @PUT("suppliers/{id}")
        Observable<NetSuccess> modifySupplier(@Path("id") int id, @Body Supplier supplier);

        @GET("suppliers/{id}/degrees")
        Observable<List<Degree>> getDegrees(@Path("id") int id);

        @POST("suppliers/{id}/degrees")
        Observable<Degree> addDegree(@Path("id") int id, @Body Degree degree);

        @POST("suppliers/{id}/jobs")
        Observable<Job> addJob(@Path("id") int id, @Body Job job);

        @GET("suppliers/{id}/skills")
        Observable<List<Skill>> getSkills(@Path("id") int id);

        @POST("suppliers/{id}/skills")
        Observable<Skill> addSkill(@Path("id") int id, @Body Skill skills);

        @GET("suppliers/{id}/portfolio")
        Observable<List<PortfolioSample>> getPortfolio(@Path("id") int id);

        @POST("suppliers/{id}/portfolio")
        Observable<PortfolioSample> addPortfolioSample(@Path("id") int id, @Body PortfolioSample portfolioSample);
    }

    public Observable<Supplier> getSupplier(int id) {
        Observable<Supplier> supplier = buildService()
                .getSupplier(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
       /* supplier.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return supplier;
    }

    public Observable<NetSuccess> modifySupplier(int id, Supplier supplier) {

        Observable<NetSuccess> success = buildService()
                .modifySupplier(id, supplier)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
       /* success.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return success;
    }

    public Observable<List<Degree>> getDegrees(int supplierId) {
        Observable<List<Degree>> degrees = buildService()
                .getDegrees(supplierId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        /*degrees.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return degrees;
    }

    public Observable<Degree> addDegree(int supplierId, Degree dataDegree) {
        Observable<Degree> degree = buildService()
                .addDegree(supplierId, dataDegree)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
       /* degree.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return degree;
    }

    public Observable<Job> addJob(int supplierId, Job dataJob) {
        Observable<Job> job = buildService()
                .addJob(supplierId, dataJob)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
        /*job.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return job;
    }

    public Observable<List<Skill>> getSkills(int supplierId) {
        Observable<List<Skill>> skills = buildService()
                .getSkills(supplierId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
       /* skills.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return skills;
    }

    public Observable<Skill> addSkill(int supplierId, Skill dataSkill) {
        Observable<Skill> skill = buildService()
                .addSkill(supplierId, dataSkill)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
       /* skill.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);*/
        return skill;

    }

    public Observable<List<PortfolioSample>> getPortfolio(int supplierId) {
        return buildService()
                .getPortfolio(supplierId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }

    public Observable<PortfolioSample> addPortfolioSample(int supplierId, PortfolioSample portfolioSample) {
        return buildService()
                .addPortfolioSample(supplierId, portfolioSample)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::networkErrorEmitter);
    }
}