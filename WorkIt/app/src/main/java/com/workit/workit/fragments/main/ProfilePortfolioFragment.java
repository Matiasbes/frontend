package com.workit.workit.fragments.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.PortfolioListAdapter;
import com.workit.workit.models.PortfolioSample;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.utilities.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfilePortfolioFragment extends BaseUserFragment implements PortfolioListAdapter.PortfolioDataProvider {

    @BindView(R.id.list_items)
    ListView listView;
    @BindView(R.id.empty_state_view)
    LinearLayout emptyViewContainer;

    private Supplier user;
    private PortfolioListAdapter adapter;

    public ProfilePortfolioFragment() {

    }

    public static ProfilePortfolioFragment newInstance(User user) {
        ProfilePortfolioFragment fragment = new ProfilePortfolioFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.USER_INTENT_KEY, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = (Supplier) getArguments().getSerializable(Constants.USER_INTENT_KEY);
        adapter = new PortfolioListAdapter(getActivity(), this, false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_portfolio, container, false);
        ButterKnife.bind(this, view);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(
                R.id.empty_state_view,
                EmptyViewFragment.newInstance("Portfolio vacío", "No se han agregado muestras al portfolio"),
                "emptyView").commit();

        listView.setAdapter(adapter);
        updateInterface();

        return view;
    }

    private void updateInterface() {
        if (user == null || user.getPortfolio() == null || user.getPortfolio().isEmpty()) {
            listView.setVisibility(View.GONE);
            emptyViewContainer.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.VISIBLE);
            emptyViewContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public List<PortfolioSample> getPortfolioSamples() {
        return user.getPortfolio();
    }

    public void editPortfolioSample(PortfolioSample portfolioSample) {

    }

    public void removePortfolioSample(PortfolioSample portfolioSample) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter == null)
            return;
        updateInterface();
        adapter.notifyDataSetChanged();
    }

    public void updateContent(User user) {
        this.user = (Supplier) user;
        if (listView != null)
            updateInterface();
    }
}
