package com.workit.workit.fragments.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.workit.workit.R;
import com.workit.workit.adapters.profile.ProfileDegreeListAdapter;
import com.workit.workit.adapters.profile.ProfileJobListAdapter;
import com.workit.workit.models.Degree;
import com.workit.workit.models.Job;
import com.workit.workit.models.Skill;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.utilities.Constants;
import com.workit.workit.utilities.Helpers;

public class ProfileInfoFragment extends BaseUserFragment {

    private Supplier user;
    private boolean shouldShowContactActions;

    public @BindView(R.id.scroll_view) ScrollView scrollView;
    @BindView(R.id.basic_info_card_view) CardView basicInfoCardView;
    @BindView(R.id.skills_card_view) CardView skillsCardView;
    @BindView(R.id.jobs_card_view) CardView jobsCardView;
    @BindView(R.id.degrees_card_view) CardView degreesCardView;

    @BindView(R.id.skills_list) ListView skillsListView;
    @BindView(R.id.jobs_list) ListView jobsListView;
    @BindView(R.id.degrees_list) ListView degreesListView;

    public ProfileInfoFragment() {

    }

    public static ProfileInfoFragment newInstance(User user, boolean shouldShowContactActions) {
        ProfileInfoFragment fragment = new ProfileInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.USER_INTENT_KEY, user);
        args.putBoolean("shouldShowContactActions", shouldShowContactActions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (Supplier) getArguments().getSerializable(Constants.USER_INTENT_KEY);
            shouldShowContactActions = getArguments().getBoolean("shouldShowContactActions", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_profile_info, container, false);
        ButterKnife.bind(this, view);

        updateViewContent();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void updateContent(User user) {
        this.user = (Supplier) user;
        updateViewContent();
    }

    private void updateViewContent() {
        updateBasicInfoContent();
        updateSkillsContent();
        updateJobsContent();
        updateDegreesContent();
        updateScrollView();
    }

    private void updateBasicInfoContent() {
        if (basicInfoCardView == null)
            return;

        TextView rateView = (TextView) basicInfoCardView.findViewById(R.id.rate);
        TextView facebookView = (TextView) basicInfoCardView.findViewById(R.id.facebook);

        loadEmailView((TextView) basicInfoCardView.findViewById(R.id.email));
        loadPhoneView((TextView) basicInfoCardView.findViewById(R.id.phone));
        basicInfoCardView.findViewById(R.id.phone_container).setVisibility(user.getPhone() == null || user.getPhone().isEmpty() ? View.GONE : View.VISIBLE);

        if (user.getRate() > 0)
            rateView.setText("$" + user.getRate());
        else
            rateView.setText("Sin especificar");

        facebookView.setText(user.getFacebookId() != null && !user.getFacebookId().isEmpty() ? "Si" : "No");
    }

    private void loadEmailView(TextView emailView) {
        emailView.setText(user.getEmail());
        if (shouldShowContactActions) {
            emailView.setTextColor(getResources().getColor(R.color.colorPrimary));
            emailView.setPaintFlags(emailView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            emailView.setOnClickListener(view -> Helpers.loadEmailClients(getActivity(), user.getEmail(), "", ""));
        }
    }

    private void loadPhoneView(TextView phoneView) {
        phoneView.setText(user.getPhone());
        if (shouldShowContactActions) {
            phoneView.setTextColor(getResources().getColor(R.color.colorPrimary));
            phoneView.setPaintFlags(phoneView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            phoneView.setOnClickListener(view -> Helpers.makeCall(getActivity(), user.getPhone()));
        }
    }



    private void updateSkillsContent() {
        if (skillsCardView == null)
            return;

        List<Skill> skills = user.getSkills();
        if (skills == null || skills.isEmpty())
            skillsCardView.setVisibility(View.GONE);
        else {
            skillsCardView.setVisibility(View.VISIBLE);
            ArrayList<String> skillsTitles = new ArrayList<>();
            for (Skill skill: skills)
                skillsTitles.add(skill.getTitle());

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, skillsTitles);
            skillsListView.setAdapter(adapter);
            Helpers.setListViewHeightBasedOnChildren(skillsListView);
        }
    }

    private void updateJobsContent() {
        if (jobsCardView == null)
            return;

        List<Job> jobs = user.getJobs();
        if (jobs == null || jobs.isEmpty())
            jobsCardView.setVisibility(View.GONE);
        else {
            jobsCardView.setVisibility(View.VISIBLE);
            ProfileJobListAdapter adapter = new ProfileJobListAdapter(getActivity(), user);
            jobsListView.setAdapter(adapter);
            Helpers.setListViewHeightBasedOnChildren(jobsListView);
        }
    }

    private void updateDegreesContent() {
        if (degreesCardView == null)
            return;

        List<Degree> degrees = user.getDegrees();
        if (degrees == null || degrees.isEmpty())
            degreesCardView.setVisibility(View.GONE);
        else {
            degreesCardView.setVisibility(View.VISIBLE);
            ProfileDegreeListAdapter adapter = new ProfileDegreeListAdapter(getActivity(), user);
            degreesListView.setAdapter(adapter);
            Helpers.setListViewHeightBasedOnChildren(degreesListView);
        }
    }

    private void updateScrollView() {
        if (scrollView == null)
            return;

        scrollView.fullScroll(View.FOCUS_UP);
        scrollView.scrollTo(0, 0);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        updateScrollView();
    }

}