package com.workit.workit.activities.profile.clients.suppliers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.workit.workit.R;
import com.workit.workit.activities.projects.ProjectsMapsActivity;
import com.workit.workit.adapters.projects.ProjectsSupplierAssignmentListAdapter;
import com.workit.workit.gcm.configurations.NotificationActivity;
import com.workit.workit.models.ProjectLocation;
import com.workit.workit.models.Project;
import com.workit.workit.models.Supplier;
import com.workit.workit.models.User;
import com.workit.workit.services.ProjectService;
import com.workit.workit.utilities.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApplyForProjectActivity extends NotificationActivity
        implements ProjectsSupplierAssignmentListAdapter.ProjectsSupplierAssignmentDataProvider {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_items)
    ListView listItems;
    @BindView(R.id.container_view)
    RelativeLayout containerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    User supplier;
    private List<Project> projects;
    private List<Project> selectedProjects;

    private ProgressDialog progressDialog;
    private ProjectsSupplierAssignmentListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_for_project);
        ButterKnife.bind(this);

        supplier = (User) getIntent().getSerializableExtra(Constants.USER_INTENT_KEY);
        selectedProjects = new ArrayList<>();

        adapter = new ProjectsSupplierAssignmentListAdapter(this, this);
        listItems.setAdapter(adapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Buscar Proyectos");

        containerView.setVisibility(View.GONE);
        setupProgressDialog();
        setupListView();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(listener -> finish());

        loadProjectsForSupplierAssignment();
    }

    private void setupProgressDialog() {
        progressDialog = new ProgressDialog(this);
    }

    private void setupListView() {
        listItems.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
    }

    private void loadProjectsForSupplierAssignment() {
        new ProjectService(this)
                .getProjects()
                .subscribe(
                        theProjects -> {
                            if (theProjects.isEmpty()) {
                                handleErrorFinish(new Throwable("No hay proyectos registrados en la plataforma"), "Sin proyectos");
                            } else {
                                projects = theProjects;
                                progressBar.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                                containerView.setVisibility(View.VISIBLE);
                            }
                        },
                        error -> handleError(error, "Operación falló"));
    }

    private void handleError(Throwable error, String title) {
        new AlertDialog
                .Builder(ApplyForProjectActivity.this)
                .setTitle(title)
                .setMessage(error.getMessage())
                .setPositiveButton("OK", null)
                .show();
        progressDialog.hide();
    }

    private void handleErrorFinish(Throwable error, String title) {
        new AlertDialog
                .Builder(ApplyForProjectActivity.this)
                .setTitle(title)
                .setMessage(error.getMessage())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ApplyForProjectActivity.this.finish();
                    }
                })
                .show();
        progressDialog.hide();
    }

    private void presentProgressDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void performProjectsSelection(View view) {
        if (selectedProjects.size() == 0)
            handleError(new Throwable("Debes seleccionar por lo menos un proyecto"), "Error");
        else
            approveRequest();
    }

    private void approveRequest() {
        presentProgressDialog("Aprobando pedido...");
        new ProjectService(this)
                .applayProjects(selectedProjects)
                .subscribe(
                        success -> operationFinishedSuccessfully(),
                        error -> handleError(error, "Operación falló"));
    }

    private void operationFinishedSuccessfully() {
        Intent intent = new Intent();
        intent.putExtra(Constants.USER_INTENT_KEY, supplier);
        setResult(RESULT_OK, intent);

        progressDialog.hide();
        progressDialog.dismiss();
        finish();
    }

    public List<Project> getProjectsForSupplierAssignment() {
        return projects == null ? new ArrayList<>() : projects;
    }

    public void filterProjects(ProjectLocation projectLocation) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText range = new EditText(getApplicationContext());
        alert.setMessage("Introduce el rango de busqueda");
        alert.setTitle("Rango (km)");
        range.setTextColor(Color.BLACK);
        alert.setView(range);
        range.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                double rangeValue = Double.parseDouble(range.getText().toString());
                performFilterProjects(rangeValue, projectLocation);
            }
        });

        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

    public void performFilterProjects(double range, ProjectLocation projectLocation) {
        new ProjectService(ApplyForProjectActivity.this)
                .GetProjectsByDistance(range, projectLocation)
                .subscribe(
                        theProjects -> {
                            if (theProjects.isEmpty()) {
                                handleError(new Throwable("No hay proyectos registrados en el rango aportado"), "Sin proyectos");
                            } else {
                                projects = theProjects;
                                progressBar.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                                containerView.setVisibility(View.VISIBLE);
                            }
                        },
                        error -> handleError(error, "Operación falló"));
    }

    public void projectSelected(Project project) {
        selectedProjects.add(project);
    }

    public void projectDeselected(Project project) {
        selectedProjects.remove(project);
    }

    public void getUserLocation(View view){
        Intent intent = new Intent(this,ProjectsMapsActivity.class);
        intent.putExtra("shouldOnlyUserLocation",true);
        this.startActivityForResult(intent, Constants.MAP_ACTION_RETURN_CODE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        if(requestCode == Constants.MAP_ACTION_RETURN_CODE){
            ProjectLocation userProjectLocation = (ProjectLocation) data.getSerializableExtra(Constants.USER_LOCATION);
            if(userProjectLocation == null) {
                Toast.makeText(this, "No selecciono ninguna ubicación", Toast.LENGTH_SHORT);
                return;
            }
            filterProjects(userProjectLocation);
        }
    }
}
