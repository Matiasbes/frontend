package com.workit.workit.fragments.main;

import android.content.Context;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    private OnBaseFragmentInteractionListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBaseFragmentInteractionListener)
            listener = (OnBaseFragmentInteractionListener) context;
        else
            throw new RuntimeException(context.toString() + " must implement OnBaseFragmentInteractionListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    protected void changeActivityTitle(String title) {
        listener.changeTitle(title);
    }

    public interface OnBaseFragmentInteractionListener {
        void changeTitle(String title);
    }

}
