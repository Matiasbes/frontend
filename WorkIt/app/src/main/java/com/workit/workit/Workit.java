package com.workit.workit;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.workit.workit.gcm.configurations.WorkItGcmReceiverData;
import com.workit.workit.gcm.configurations.WorkItGcmReceiverUIBackground;
import com.workit.workit.utilities.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx_gcm.internal.RxGcm;

/**
 * Created by Matías on 5/29/2017.
 */

public class Workit extends Application {

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        setupFresco();
        setupGcm();
    }

    private void setupFresco() {
        sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
                .newBuilder(this, getHttpClient())
                .build();
        Fresco.initialize(this, config);

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearCaches();
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                if (sharedPreferences.contains(Constants.TOKEN_PREFERENCES_KEY)) {
                    Request request = chain.request().newBuilder()
                            .header("Authorization", "Basic " + sharedPreferences.getString(Constants.TOKEN_PREFERENCES_KEY, ""))
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                }

                return chain.proceed(original);
            }
        });
        return httpClient.build();
    }
    private void setupGcm() {
        RxGcm.Notifications
                .register(this, WorkItGcmReceiverData.class, WorkItGcmReceiverUIBackground.class)
                .subscribe(
                        token -> System.out.println("Token received: " + token),
                        error -> System.out.println("GCM registration error: " + error)
                );
    }
}
