package com.workit.workit.fragments.main;

import android.support.v4.app.Fragment;

import com.workit.workit.R;
import com.workit.workit.models.User;

public abstract class BaseUserFragment extends Fragment {

    public abstract void updateContent(User user);

}