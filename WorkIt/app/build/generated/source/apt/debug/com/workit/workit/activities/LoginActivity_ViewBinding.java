// Generated code from Butter Knife. Do not modify!
package com.workit.workit.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target, View source) {
    this.target = target;

    target.info = Utils.findRequiredViewAsType(source, 2131689706, "field 'info'", TextView.class);
    target.emailView = Utils.findRequiredViewAsType(source, 2131689570, "field 'emailView'", EditText.class);
    target.passwordView = Utils.findRequiredViewAsType(source, 2131689711, "field 'passwordView'", EditText.class);
    target.textInputLayout = Utils.findRequiredViewAsType(source, 2131689710, "field 'textInputLayout'", TextInputLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.info = null;
    target.emailView = null;
    target.passwordView = null;
    target.textInputLayout = null;
  }
}
